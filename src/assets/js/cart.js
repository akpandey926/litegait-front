var cart = null;	

$( document ).ready(function() {

	cart = JSON.parse(localStorage.getItem("mycart"));

	if(cart==null)
	{	
		cart = new Object();
		cart.subtotal= 0;
		cart.discount= 0;
		cart.discountcode= '';
		cart.totalqty= 0;
		cart.items=new Array();		
	}	

	display_cart_cnt();		

	$(document).on('click','.addcart',function() {		
		var check_plan = ''; 
		check_plan = $(this).attr('check_plan'); 
		if(check_plan){			
			return false; 
		}
		// localStorage.removeItem("mycart")
		cart = JSON.parse(localStorage.getItem("mycart"));		
		if(cart==null)
		{	
			cart = new Object();
			cart.subtotal= 0;
			cart.discount= 0;
			cart.discountcode= '';
			cart.totalqty= 0;
			cart.items=new Array();			
		}

		var id= $(this).attr('data-id');
		var slug= $(this).attr('data-slug');
		var name= $(this).attr('data-name');
		var image= $(this).attr('data-image');
		var price= $(this).attr('data-price');
		var type= $(this).attr('data-ptype');
		var category= $(this).attr('data-category');
		var categorySlug= $(this).attr('data-categoryslug');
		var datetime= $(this).attr('data-datetime');
		var dateTimeReal= $(this).attr('data-datetimereal');
		var qty= 1;		
		
		var item = new Object();
		item.id = id;
		item.slug = slug;
		item.name = name;
		item.image = image; 
		item.datetime = datetime;
		item.dateTimeReal = dateTimeReal;
		item.type = type;
		item.category = category;
		item.categorySlug = categorySlug;
		item.price = Number(price);
		item.qty = 1;
		item.total = Number(price);
		
		/*
		var check_cart_webinar = false ; 
		if(cart && cart.items.length > 0){
	        cart.items.forEach(function(message){
	            // console.log(message.type); 
	            if(message.type == "webinar" && item.type != 'webinar'){
	              check_cart_webinar = true; 
	            }
	        });
		}

		if(check_cart_webinar){
			localStorage.removeItem("mycart"); 
			cart = new Object();
			cart.subtotal= 0;
			cart.discount= 0;
			cart.discountcode= '';
			cart.totalqty= 0;
			cart.items=new Array();	
		}
		*/ 
		var already=false;
		$.each(cart.items, function (key, value) {		
			if(value.id==id)
			already=true;			
		});			
		
		if(already==false)
		{
			items=new Array();
			items = cart.items;			
			items.push(item);			
			cart.items= items;				
			update_cart();		
			display_cart_cnt();	
		}
		jQuery.noConflict(); 		
		var gallery_path = $('#addtocartModal .addcartpopup .purchase_item_img').attr( 'img_attr'); 
		$('#addtocartModal .addcartpopup .purchase_item_img').attr( 'src', gallery_path+'/'+item.image); 
		$('#addtocartModal .addcartpopup .prod_title').text(item.name); 
		make_cartpopup_html1 =  make_cartpopup_html(cart, gallery_path); 
		$('#addtocartModal .addcartpopup .cartsection').empty(); 
		$('#addtocartModal .addcartpopup .cartsection').append(make_cartpopup_html1); 
		
		/*
		var check_cart_prod = false ; 
		if(cart && cart.items.length > 0){
	        cart.items.forEach(function(message){
	            // console.log(message.type); 
	            if(message.type == "product" || message.type == "seminar" || message.type == "part"){
	              check_cart_prod = true; 
	            }
	        });
		}
		*/


		if (!check_plan) {
			$('#addtocartModal').modal('show');
		}

	});

	$(document).on('click','.close_addcart_success',function() {		
		$(".addcart_success").slideUp();			
	});
});


function make_cartpopup_html(cart, gallery_path){
	
	var str = '<div class="row">'; 
	$.each( cart.items, function( key, value ) {
		str += '<div class="col-sm-12"> <hr></div>'; 
		str += '<div class="col-sm-12">'; 
		str += '<div class="col-sm-4 ">'; 
		str += '<img width="40px" height="40px" src="'+gallery_path+value.image+'"/>'; 
		str += '</div>'; 
		str += '<div class="col-sm-6">'+ value.name + '</div>';
		str += '<div class="col-sm-2">'+value.price+'</div>'
		str += '</div>'; 
	});
	str +=  ' </div><hr class="subtotal">'; 
	str += '<div><strong style="float: left;">'+cart.totalqty+ ' item' + (cart.totalqty > 1 ? 's' : '') + ' in cart</strong>'                                                          
	str += ' <strong style="float: right;">subtotal: $'+ cart.subtotal + '</strong></div>'       
    return str; 
}

             
function update_cart(){

	var items = cart.items;	
	var total=0;
	var totalqty=0;
	$.each(items, function (key, value) {
		total = total + (value.price * value.qty);
		totalqty++;
	});
	cart.subtotal=total;
	cart.total=total;
	cart.totalqty=totalqty;		
	
	localStorage.setItem("mycart", JSON.stringify(cart)); 
}

function display_cart_cnt(){

	var len = cart.items.length;
	if(len >= 1)
	{
		$('span.cartcnt').show().html(len);		
	}
	else
	{
		$('span.cartcnt').hide();
	}	
}

