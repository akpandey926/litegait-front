import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stringfilter'
})
export class StringfilterPipe implements PipeTransform {

  	/* transform(items: any[], field: string, value: string): any[] {
        if (!items) {
            return [];
        }
        if (!field || !value) {
            return items;
        }
        return items.filter(singleItem => singleItem[field].toLowerCase().includes(value.toLowerCase()));
    }*/

    transform(items: any, filter: any, isAnd: boolean,filterMetadata): any {
    	// console.log(items); console.log(filter);  
		if (filter && Array.isArray(items)) {
		  	let filterKeys = Object.keys(filter);

		  	if (isAnd) {
		    	var filteredItems = items.filter(item =>
		        	filterKeys.reduce((memo, keyName) =>
		            	(memo && new RegExp(filter[keyName], 'gi').test(item[keyName])) || filter[keyName] === "", true));

		    	filterMetadata.count = filteredItems.length;
		    	return filteredItems;
		  	} else {

			  	if(filter.tag &&  Array.isArray(filter.tag) &&  filter.tag.length > 0){
			    	var filteredItems = items.filter(item => {
			    			var res = item.tag.filter(function(n) { return filter.tag.indexOf(n) > -1; });
			    			return res.length > 0 ? true : false; 
			    	});
			    	filterMetadata.count = filteredItems.length;			    	 
			    	return filteredItems;

			  	}else if(filter.tag &&  Array.isArray(filter.tag) &&  filter.tag.length <= 0){
			  		var filteredItems = items; 
					filterMetadata.count = filteredItems.length;					
			    	return filteredItems;
			  	}else if(filter.topics &&  Array.isArray(filter.topics) &&  filter.topics.length > 0){  // for webinar page 
				  		var filteredItems = items.filter(item => {
				    			var res = item.topics.filter(function(n) { return filter.topics.indexOf(n) > -1; });
				    			return res.length > 0 ? true : false; 
				    	});
				    	filterMetadata.count = filteredItems.length;				    	
				    	return filteredItems;					

			  	}else if(filter.topics &&  Array.isArray(filter.topics) &&  filter.topics.length <= 0){ // for webinar page empty topic array
			    	var filteredItems = items; 
					filterMetadata.count = filteredItems.length;					
			    	return filteredItems;
			    }else{
			    	var filteredItems = items.filter(item => {
			      	return filterKeys.some((keyName) => {			        	
			        	return new RegExp(filter[keyName], 'gi').test(item[keyName]) || filter[keyName] === "";});
			    	});

			    	filterMetadata.count = filteredItems.length;
			    	return filteredItems;
			    }
		  	}
		} else {
		  return items;
		}
  	}

}
