import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'webinarsearch'
})
export class WebinarsearchPipe implements PipeTransform {

  transform(items: any, filter: any, isAnd: boolean, filterMetadata): any {
    var filteredItems = items;
    if (filter && Array.isArray(items)) {
      let filterKeys = Object.keys(filter);

      if (filter.tag && Array.isArray(filter.tag) && filter.tag.length <= 0) {
        filterMetadata.count = filteredItems.length;
      }
      if (filter.topics && Array.isArray(filter.topics) && filter.topics.length > 0) {  // for webinar page
        filteredItems = filteredItems.filter(item => {
          var res = item.topics.filter(function (n) {
            return filter.topics.indexOf(n) > -1;
          });
          return res.length > 0 ? true : false;
        });
        filterMetadata.count = filteredItems.length;
      }

      filteredItems = filteredItems.filter(item => {
        if (item.title && filter && filter.title != "") {
          let title = item.title.replace(/\s+/g, ' ').trim().toLowerCase();
          let description = item.description.replace(/\s+/g, ' ').trim().toLowerCase();
          let objective = item.objective.replace(/\s+/g, ' ').trim().toLowerCase();
          if (title.includes(filter.title.toLowerCase()) || description.includes(filter.description.toLowerCase()) || objective.includes(filter.objective.toLowerCase()))
            return item;
        }
        else {

          return item
        }
      });

      filterMetadata.count = filteredItems.length;
      return filteredItems;
    } else {
      return filteredItems;
    }
  }

}
