import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class UserService {

	constructor(private http: HttpClient) { }	

  sendToken(token){
    return this.http.post(environment.apiUrl+'/users/token_validate',{recaptcha: token});  
  }


	addUser(user) { 
		return this.http.post(environment.apiUrl+'/users/add',user);   		
    } 

    sendForgotPasswordMail(user) { 
        return this.http.post(environment.apiUrl+'/users/send_forgot_password_mail',user);           
    } 

    resetUserPassword(user){
        return this.http.post(environment.apiUrl+'/users/reset_user_password',user);       
    }

    
    addGuestUser(user) { 

        return this.http.post(environment.apiUrl+'/users/add_guest',user);           
    } 

    loginUser(user) { 
		return this.http.post(environment.apiUrl+'/users/login',user);   		
    }  

    getUser(user_id) { 
		return this.http.get(environment.apiUrl+'/users/getuser/'+user_id);   		
    } 

    getUserPlan(user_id){
        return this.http.get(environment.apiUrl+'/users/get_user_plan/'+user_id);           
    }    
    getUserELPHistory(user_id){
        return this.http.get(environment.apiUrl+'/users/get_user_elp_history/'+user_id);           
    }    

    saveUser(user) { 
		return this.http.post(environment.apiUrl+'/users/saveuser',user);   		
    }

    changePassword(user) { 
		return this.http.post(environment.apiUrl+'/users/changepassword',user);   		
    }

    getOrders(user_id){
        return this.http.get(environment.apiUrl+'/users/orders/'+user_id);
    }

    getOrder(order_id){
        return this.http.get(environment.apiUrl+'/users/order/'+order_id);
    }

    getWebinars(user_id){
        return this.http.get(environment.apiUrl+'/users/webinars/'+user_id);
    }
    getSeminars(user_id){
        return this.http.get(environment.apiUrl+'/users/seminars/'+user_id);
    }

    getCertificate(webinar_id,user_id){ 
    
      return this.http.get(environment.apiUrl+'/users/get-certificate/'+webinar_id+'/'+user_id);
    }

}
