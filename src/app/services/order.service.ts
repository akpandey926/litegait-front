import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment';
import { AuthGuard } from './authguard.service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
	
	languageCode='en';
	regionCode='us';

  	constructor(private http: HttpClient, private authGuard: AuthGuard) { }

   	saveOrder(orderData) {             
		  return this.http.post(environment.apiUrl+'/order/save',orderData);   		
    }

    checkCouponCode(couponCode) {  
    	var user = this.authGuard.getUser(); 
    	var cart = JSON.parse(localStorage.getItem("mycart"));          
		return this.http.post(environment.apiUrl+'/order/check_coupon_code',{'couponCode':couponCode,'user':user,'cart':cart});   		
    }
    getStates(country_id) { 
		return this.http.get(environment.apiUrl+'/users/getState/'+country_id);   		
	} 
	getUserDefaultAddress(user_id) { 
		return this.http.get(environment.apiUrl+'/order/getUserDefaultAddress/'+user_id);   		
    } 
}
