import { Component, OnInit } from '@angular/core';
import { Router,NavigationEnd, UrlSegment, UrlSegmentGroup,PRIMARY_OUTLET,UrlTree} from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  title = 'litegait';
  isLoginRoute;
  sidebar: UrlSegmentGroup;
  constructor (private router: Router) {   
    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {

        const tree: UrlTree = router.parseUrl(e.url); 
        const g: UrlSegmentGroup = tree.root.children[PRIMARY_OUTLET];

        if(g){
          const s: UrlSegment[] = g.segments;
          this.isLoginRoute = s[0].path;
        }
        
      }
    });
  }
  
}