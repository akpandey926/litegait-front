import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { StaticpageComponent } from './components/staticpage/staticpage.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { NewsComponent } from './components/news/news.component';
import { FaqComponent } from './components/faq/faq.component';
import { GalleryComponent } from './components/gallery/gallery.component';
import { BlogComponent } from './components/blog/blog.component';
import { CategorylistComponent } from './components/categorylist/categorylist.component';
import { CategoryComponent } from './components/category/category.component';
import { ProductlistComponent } from './components/productlist/productlist.component';
import { ProductComponent } from './components/product/product.component';
import { CartComponent } from './components/cart/cart.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { GuestcheckoutComponent } from './components/guestcheckout/guestcheckout.component';
import { ForgotpasswordComponent } from './components/user/forgotpassword/forgotpassword.component';
import { ResetpasswordComponent } from './components/user/resetpassword/resetpassword.component';


import { PaymentComponent } from './components/payment/payment.component';
import { RequestquoteComponent } from './components/requestquote/requestquote.component';
import { RequestserviceComponent } from './components/requestservice/requestservice.component';
import { DashboardComponent } from './components/user/dashboard/dashboard.component';
import { ChangepasswordComponent } from './components/user/changepassword/changepassword.component';
import { MyprofileComponent } from './components/user/myprofile/myprofile.component';
import { MyorderComponent } from './components/user/myorder/myorder.component';
import { MyseminarComponent } from './components/user/myseminar/myseminar.component';
import { MywebinarComponent } from './components/user/mywebinar/mywebinar.component';
import { MywebinarcertificateComponent } from './components/user/mywebinarcertificate/mywebinarcertificate.component';


import { PartlistComponent } from './components/partlist/partlist.component';
import { PartComponent } from './components/part/part.component';
import { InstructorlistComponent } from './components/instructorlist/instructorlist.component';
import { InstructorComponent } from './components/instructor/instructor.component';
import { WebinarlistComponent } from './components/webinarlist/webinarlist.component';
import { WebinarComponent } from './components/webinar/webinar.component';
import { SeminarlistComponent } from './components/seminarlist/seminarlist.component';
import { SeminarComponent } from './components/seminar/seminar.component';
import { RequesthostseminarComponent } from './components/requesthostseminar/requesthostseminar.component';
import { ElppaymentComponent } from './components/elppayment/elppayment.component';
import { ServiceComponent } from './components/service/service.component';
import { ServicedocumentsComponent } from './components/service/servicedocuments/servicedocuments.component';
import { ServiceproductComponent } from './components/service/serviceproduct/serviceproduct.component';
import { ServicecontactusComponent } from './components/service/servicecontactus/servicecontactus.component';
import { BulletinComponent } from './components/bulletin/bulletin.component';
import { EducationComponent } from './components/education/education.component';
import {DistributerComponent} from './components/distributer/distributer.component';

import { AuthGuard } from './services/authguard.service';
import { IsSecureGuard } from './services/secure-guard.service';
import { from } from 'rxjs';

const routes: Routes = [
	{ path: '', component: HomeComponent},
	{ path: 'home', component: HomeComponent},
	{ path: 'changeLanguage/:languageCode/:languageName', component: HeaderComponent},
	{ path: 'changeRegion/:regionCode', component: HeaderComponent},
	{ path: 'page/:slug', component: StaticpageComponent },
	{ path: 'education', component: EducationComponent },
	{ path: 'register', component: RegisterComponent },
	{ path: 'forgot-password', component: ForgotpasswordComponent },
	{ path: 'reset_password/:reset_token', component: ResetpasswordComponent },

	{ path: 'login', component: LoginComponent },
	{ path: 'login/:type', component: LoginComponent, data: {page: 'checkout'} },
	{ path: 'logout', component: LoginComponent, data: {page: 'logout'}},
	{ path: 'news', component: NewsComponent },
	{ path: 'news/:slug', component: NewsComponent },
	{ path: 'faqs', component: FaqComponent },
	// { path: 'faqs/:id', component: FaqComponent },
	{ path: 'faqs/:pagetype/:slug', component: FaqComponent },
	{ path: 'gallery', component: GalleryComponent },
	{ path: 'gallery/:slug', component: GalleryComponent },
	{ path: 'blogs', component: BlogComponent },
	{ path: 'blog/:slug', component: BlogComponent },
	{ path: 'products', component: CategorylistComponent },
	// { path: 'category/:slug', component: CategoryComponent },
	{ path: 'category/:slug', component: ProductlistComponent },
	{ path: 'productlist/:slug', component: ProductlistComponent },
	{ path: 'product/:slug', component: ProductComponent },
	{ path: 'cart', component: CartComponent },
	{ path: 'checkout', component: CheckoutComponent, canActivate: [AuthGuard, IsSecureGuard]},
	{ path: 'checkout-as-guest', component: GuestcheckoutComponent},

	{ path: 'payment', component: PaymentComponent },
	{ path: 'request_quote/:type/:slug', component: RequestquoteComponent },
	{ path: 'request_service/:type/:slug', component: RequestserviceComponent },
	{ path: 'parts', data: {page: 'parts'} ,
		children: [
			{path: '', component: PartlistComponent},
			{path: ':category', component: PartlistComponent},
	    	{path: ':product/:category', component: PartlistComponent},
		]
	},
	{ path: 'accessories', data: {page: 'accessories'} ,
		children: [
			{path: '', component: PartlistComponent},
			{path: ':category', component: PartlistComponent},
	    	{path: ':product/:category', component: PartlistComponent},
		]
	},
	{ path: 'part/:slug', component: PartComponent },
	{
	  	path: 'user', canActivate: [AuthGuard],
	  	children: [
	    	{path: 'dashboard', component: DashboardComponent},
			{path: 'webinar', component: MywebinarComponent},
			
	    	{path: 'seminar', component: MyseminarComponent},
	    	{path: 'order/:order_id', component: MyorderComponent},
	    	{path: 'myprofile', component: MyprofileComponent},
	    	{path: 'changepassword', component: ChangepasswordComponent},
	  ]
	},
	{path: 'webinar-certificate/:webinar_id/:user_id', component: MywebinarcertificateComponent},
	{ path: 'instructors', component: InstructorlistComponent },
	{ path: 'instructor/:slug', component: InstructorComponent },
	{path: 'webinars',
		children: [
			{path: '', component: WebinarlistComponent},
			{path: ':type', component: WebinarlistComponent},
			{path: ':type/:category', component: WebinarlistComponent},
			{path: ':type/:category/:subcat', component: WebinarlistComponent},
		]
	},
	{ path: 'webinar/:slug', component: WebinarComponent },
	{ path: 'seminars', component: SeminarlistComponent },
	{ path: 'seminar/:slug', component: SeminarComponent },
	{ path: 'request_host_seminar/:slug', component: RequesthostseminarComponent },
	{ path: 'elp-payment/:elp_id/:price', component: ElppaymentComponent , canActivate: [AuthGuard]},	
	{path: 'service',
		children: [
			{path: '', component: ServiceComponent},
			{path: 'documents', component: ServicedocumentsComponent},
			{path: 'documents/:category', component: ServicedocumentsComponent},
			{path: 'product/:slug', component: ServiceproductComponent},
			{path: 'contact-us', component: ServicecontactusComponent},
		]
	},
	{ path: 'newsletter', component: BulletinComponent},		
	{ path: 'distributer', component: DistributerComponent},		
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
