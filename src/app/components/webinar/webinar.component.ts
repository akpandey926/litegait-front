import { Component, OnInit } from '@angular/core';
import { Title,Meta }  from '@angular/platform-browser';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

import { AuthGuard } from '../../services/authguard.service';
import { environment } from '../../../environments/environment';
import { WebinarService } from '../../services/webinar.service';
import { UserService } from '../../services/user.service';
import * as moment from 'moment-timezone';
//const moment = require('moment-timezone');
import * as $ from 'jquery';
@Component({
  selector: 'app-webinar',
  templateUrl: './webinar.component.html',
  styleUrls: ['./webinar.component.css']
})
export class WebinarComponent implements OnInit {

    cart: any = [];
  	env = environment;
  	webinar:any = []; 
  	categories:any = [];
  	states:any = [];
  	banner_image=''; 
  	approved_state_txt="";     	
  	approved_state_txt_false="";   
    state_msg:any = {}; 
    user; 
    check_plan;   
    check_cart_prod:boolean = false; 
    data; 	
    error = '';
    is_gold_plan: boolean = false; 
    plandata: any = {}; 
    state_default_msg: any = {}; 
    recording: boolean = false; 
	TZNameDateTime1:any;
	TZNameDateTime2:any;
	checkPurchasedWebinar = true;
	//timeZone = "America/new_york";
	
	timeZone = moment.tz.guess();
  	constructor(private userService: UserService, private _flashMessagesService: FlashMessagesService, private webinarService: WebinarService,  private authGuard: AuthGuard , private titleService: Title,private meta: Meta,private router: Router, private activatedRoute: ActivatedRoute) { 
  	}

  	ngOnInit() {
		
    	this.user = this.authGuard.getUser(); 
		
        var slug = this.activatedRoute.snapshot.paramMap.get('slug');
        this.webinarService.getWebinarView(slug).subscribe((response:any) => { 
			this.webinar = response.data;
			var webinarDateTime1 = moment(this.webinar.date_time_1);
			var webinarDateTime2 = moment(this.webinar.date_time_2);

			this.TZNameDateTime1 = webinarDateTime1.tz(this.timeZone).format('z');
			this.TZNameDateTime2 = webinarDateTime2.tz(this.timeZone).format('z');
			if(new Date(this.webinar.date_time_1).getTime() <= new Date().getTime() && new Date(this.webinar.date_time_2).getTime() <= new Date().getTime()){
  				this.recording = true;
  			}	
        	if(this.webinar.state_msg)
        		this.state_msg = JSON.parse(this.webinar.state_msg); 

  			this.banner_image = this.webinar.category.banner_image;
  			this.categories = response.categories;  			
  			this.states = response.states;  
        	var temp_st_msg = {}; 
        	this.states.forEach(function (arrayItem) {
            	temp_st_msg[arrayItem.name] = arrayItem.default_message; 
	        });
    	    this.state_default_msg = temp_st_msg; 
        	if(response.data.meta_title)	
  				this.titleService.setTitle(environment.siteName + ' - '+response.data.meta_title);	
        	if(response.data.meta_description) 	
          		this.meta.addTag({ name: 'description', content: response.data.meta_description });  
		});
		
		this.webinarService.checkAlreadyPurchasedWebinar(slug,this.user.user_id).subscribe((response:any) => {
			this.checkPurchasedWebinar = response.data;
		});


		/* this.userService.getUserPlan(this.user.user_id).subscribe((response:any) => {
			this.plandata = response.data;     
			if(this.plandata.elp_id.persons > 1){
			  	this.is_gold_plan = true; 
			} else{
			  	this.is_gold_plan = false; 
			}
		});  
    	if(this.user.email && new Date(this.user.elp_expire).getTime() > new Date().getTime() ){
      		this.check_plan = 1; 
    	}else{
      		this.check_plan = ''; 
    	} */

    	this.cart = JSON.parse(localStorage.getItem("mycart"));        
  	}

	myClickFunction(){
		//$('.hidden_text').hide();
		$('.desc_readmore').click(function(){
			$(this).hide();
			$('.hidden_text').addClass('show');
		});
	}
  	getDateSuffix(day)
  	{
  		var suffix = 'th';       

		if (day === '1' || day === '21' || day === '31') {
		suffix = 'st';
		}
		if (day === '2' || day === '22') {
		suffix = 'nd';
		}
		if (day === '3' || day === '23') {
		suffix = 'rd';
		}
		return suffix;
  	}

  	checkState(value){

  		if(this.state_msg[value]){
          	this.approved_state_txt = ' <i class="fa fa-check"></i> '+this.state_msg[value];
   		}else if(this.state_default_msg[value]){
        	this.approved_state_txt = ' <i class="fa fa-check"></i> '+this.state_default_msg[value];
    	}
    	else
    	{
    		this.approved_state_txt = '';
    	}
    	this.approved_state_txt_false ='';

  		/*if(this.webinar.states.indexOf(value) !== -1 ) 	
  		{	
      
        if(this.state_msg[value]){
              this.approved_state_txt = ' <i class="fa fa-check"></i> '+this.state_msg[value];
        }else if(this.state_default_msg[value]){
             this.approved_state_txt = ' <i class="fa fa-check"></i> '+this.state_default_msg[value];
        }else{
              this.approved_state_txt = ' <i class="fa fa-check"></i> This course  is approved in '+value;
        }
  			this.approved_state_txt_false ='';
  		}	
  		else
  		{
  			this.approved_state_txt_false = ' <i class="fa fa-check"></i> This course  is Not approved in '+value;
  			this.approved_state_txt ='';
  		} */

  	}

    purchase_by_plan(datetime){ 
      this.cart= null ;

      if(this.user.elp_plan >=1){ // if plan already purchased
        this.data = {}; 
        this.data.user_id = this.user.user_id; 
        this.data.webinar_id = this.webinar._id; 
        this.data.datetime = datetime; 
        this.webinarService.purchaseByPlan(this.data).subscribe((response:any) => { 
            if (response["status"] == 'success') {
                this._flashMessagesService.show('Webinar purchased successfully.', { cssClass: 'alert-success', timeout: 5000 });
                this.router.navigate(['/user/webinar']);
            } else {
               this.error = response["msg"];
               this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
            }            
        });
      }
      /*
      else{
        if(this.cart && this.cart.items.length > 0){
          this.cart.items.forEach((message) => {
              // console.log(message.type); 
              if(message.type == "product" || message.type == "seminar" || message.type == "part"){
                this.check_cart_prod = true; 
              }
          });
        }

        if(!this.check_cart_prod){
          // this.router.navigate(['/payment']);
          setTimeout( () => { this.router.navigate(['/payment']) }, 500 );
        }
        // this.router.navigate(['/payment']);
      }
      */

    }

  isArray(obj : any ) {
     return Array.isArray(obj)
  }
      
}
