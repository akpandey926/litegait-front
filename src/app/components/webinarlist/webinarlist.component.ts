import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { environment } from '../../../environments/environment';
import { WebinarService } from '../../services/webinar.service';
import * as moment from 'moment-timezone';
@Component({
	selector: 'app-webinarlist',
	templateUrl: './webinarlist.component.html',
	styleUrls: ['./webinarlist.component.css']
})
export class WebinarlistComponent implements OnInit {

	env = environment;
	webinars: any = [];
	banner_image = '';
	categories: any = [];
	p = 1;
	itemsPerPage = 10;
	type = '';
	category = '';
	subcat = '';
	filterMetadata = { count: 0 };
	keyword = '';
	topics = '';
	search_topic = [];
	search_topic_arr = [];
	dropdownSettings = {};
	//timeZone = "America/new_york";
	timeZone = moment.tz.guess();
	userTzTime: any; 
	serverTime: any; 
	constructor(private webinarService: WebinarService, public titleService: Title, private router: Router, private activatedRoute: ActivatedRoute) {

		this.router.routeReuseStrategy.shouldReuseRoute = function () {
			return false;
		};
	}

	ngOnInit() {
		this.titleService.setTitle('Webinars');
		this.type = this.activatedRoute.snapshot.paramMap.get('type');
		this.category = this.activatedRoute.snapshot.paramMap.get('category');
		this.subcat = this.activatedRoute.snapshot.paramMap.get('subcat');
		
		if (this.category)
			this.titleService.setTitle(this.category + ' Webinars');
		else
			this.titleService.setTitle('Webinars');

		this.getData();
		//this.search_topic_arr.push('Php');
	}

	getData() {

		this.webinarService.getWebinarList(this.type, this.category, this.subcat).subscribe((response: any) => {
			this.categories = response.categories;
			this.topics = response.topics;
			this.webinars = response.data;
			this.serverTime = response.serverTime;
			if (this.category != null && response.data[0]) {
				this.banner_image = response.data[0].category.banner_image;
			}
		});


		this.dropdownSettings = {
			singleSelection: false,
			enableCheckAll: false,
			idField: '_id',
			textField: 'topic',
			selectAllText: 'Select All',
			unSelectAllText: 'UnSelect All',
			itemsShowLimit: 3,
			allowSearchFilter: false,
			maxHeight: 150,
		};
	}
	truncateText(source, size) {
		return source.length > size ? source.slice(0, size - 1) + "…" : source;
	}
	getTiemzone(webinartime: any) {
		var webinarDateTime1 = moment(webinartime);
		this.userTzTime = webinarDateTime1.tz(this.timeZone).format('z');
		return this.userTzTime;
	}
	onItemSelect(item: any) { 
		if (this.search_topic) {
			var i = 0;
			var temp: any = Array();
			this.search_topic.forEach(function (arrayItem) {
				temp[i] = arrayItem.topic;
				i++;
			});
			this.search_topic_arr = temp;
						
		}
	}

	onItemUnSelect(item: any) {
		if (this.search_topic) {
			var i = 0;
			var temp: any = Array();
			this.search_topic.forEach(function (arrayItem) {
				temp[i] = arrayItem.topic;
				i++;
			});
			this.search_topic_arr = temp;
		}
	}

	onSelectAll(items: any) {

	}

	checkRecording(dt,dc) {
		if(new Date(dt).getTime() > dc)
    		return false;    	
    	else
    		return true;
	}

}


