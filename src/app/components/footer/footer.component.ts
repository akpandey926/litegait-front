import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { environment } from '../../../environments/environment';
import { CommonService } from '../../services/common.service';
import { UserService } from '../../services/user.service';
import { HttpClient} from '@angular/common/http';
import { FlashMessagesService } from 'angular2-flash-messages';
import { AuthGuard } from '../../services/authguard.service';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
	
	user;
	userForm: FormGroup;
	env = environment;	
  	menu1Data:any = [];
  	menu2Data:any = [];
	error = '';

	constructor(private commonService: CommonService,private userService: UserService, private http: HttpClient, private _flashMessagesService: FlashMessagesService, private router: Router, private activatedRoute: ActivatedRoute,private authGuard: AuthGuard) { }

	ngOnInit() {
		
		this.user = this.authGuard.getUser();
		this.authGuard.change.subscribe(value => {
			this.user = this.authGuard.getUser();
		});


		this.commonService.getMenuList('footer1').subscribe((response:any) => {
  				this.menu1Data = response.data;   				      		
    	});
    	this.commonService.getMenuList('footer2').subscribe((response:any) => {
  				this.menu2Data = response.data;       		
		});
		
		this.userForm = new FormGroup({
			'email': new FormControl('', [
			  Validators.required,
			  Validators.email,
			]),
			'password': new FormControl('', [
			  Validators.required,
			  Validators.minLength(6),
			]),
	  });

	}

	public submitForm() {

		var str = this.userService.loginUser(this.userForm.value).subscribe(response => {

			if (response["status"] == 'success') {
				this.authGuard.setUser(response["data"]);
				this.router.navigate(['/user/dashboard']);	
			} else {
				this.error = response["msg"];
				setTimeout(()=>{
					this.error = "";
			   }, 10000);
			}
	  });

	}

}
