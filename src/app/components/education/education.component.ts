import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';

import { HomeService } from '../../services/home.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.css']
})
export class EducationComponent implements OnInit {

	env = environment;
	banner_image = '';
	testimonials: any = [];

	constructor(private homeService: HomeService, private router: Router, private activatedRoute: ActivatedRoute) { }

  	ngOnInit() {

		this.homeService.getTestimonails().subscribe((response: any) => {
            this.testimonials = response.data;
        });
  	}

}
