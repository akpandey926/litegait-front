import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Title,Meta }  from '@angular/platform-browser';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { FormBuilder, FormGroup, FormControl, Validators,AbstractControl } from '@angular/forms';
import { FlashMessagesService } from 'angular2-flash-messages';

import { environment } from '../../../environments/environment';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-requestquote',
  templateUrl: './requestservice.component.html',
  styleUrls: ['./requestservice.component.css']
})
export class RequestserviceComponent implements OnInit {

	env = environment;    
    product:any = [];    
    quoteForm: any = {};    
    error :'';
    type ="";
    slug ="";

  	constructor(private productService: ProductService, private router: Router, private activatedRoute: ActivatedRoute,private titleService: Title,private _flashMessagesService: FlashMessagesService) {
		
  	}

  	ngOnInit() {       

  		this.type = this.activatedRoute.snapshot.paramMap.get('type');
  		this.slug = this.activatedRoute.snapshot.paramMap.get('slug');
  		this.productService.getProductQuoteForm(this.type,this.slug).subscribe((response:any) => {             
            this.product = response.product;  
            this.titleService.setTitle(environment.siteName + ' - '+response.product.name+' - Request A Service');				
	    });
  		

  	}

  	public submitForm(){
  	  	this.quoteForm.product_id = this.product._id;
        this.quoteForm.type = this.type;
        //console.log(this.quoteForm);	 
  		var str = this.productService.saveProductServiceForm(this.quoteForm).subscribe(response => {       		
       		if(response["status"] == 'success')
       		{
       			this._flashMessagesService.show('Thank you for your Inquiry! Our service representatives will call you as soon as possible.', { cssClass: 'alert-success', timeout: 5000 });
  				if(this.type == 'product')
                    this.router.navigate(['/product/'+this.product.slug]);
                else if(this.type == 'part')
                    this.router.navigate(['/part/'+this.product.slug]);                
          }
          else
          {
  				    this.error = response["msg"];
          }	
    	});
  	}	

  	stringtoArray(str)
    {      
		if(str == undefined)
		return new Array();			
		
		var array = str.split(';');					
		return array;
    }

}
