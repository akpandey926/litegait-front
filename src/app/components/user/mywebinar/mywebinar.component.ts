import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Title,Meta }  from '@angular/platform-browser';

import { AuthGuard } from '../../../services/authguard.service';
import { environment } from '../../../../environments/environment';
import { UserService } from '../../../services/user.service';
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-mywebinar',
  templateUrl: './mywebinar.component.html',
  styleUrls: ['./mywebinar.component.css']
})
export class MywebinarComponent implements OnInit {

  	env = environment;   
  	orders;
    user;
	userTzTime:any;
	timeZone = moment.tz.guess();
  	constructor(private userService: UserService, private authGuard: AuthGuard, private titleService: Title) { }

  	ngOnInit() { 
  		this.titleService.setTitle(environment.siteName + ' - My Webinar');	
  		this.user = this.authGuard.getUser();	  		
  		this.userService.getWebinars(this.user.user_id).subscribe((response:any) => { 
	  		this.orders = response.data;	  		  		
	    });
	  }
	  
	  getIncrementserial(id){
		return id++
	  }
		getTiemzone(webinartime:any){
			var webinarDateTime1 = moment(webinartime);
			this.userTzTime = webinarDateTime1.tz(this.timeZone).format('z');
			return this.userTzTime;
		}

}
