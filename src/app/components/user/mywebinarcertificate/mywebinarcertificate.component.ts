import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Title,Meta }  from '@angular/platform-browser';

import { AuthGuard } from '../../../services/authguard.service';
import { environment } from '../../../../environments/environment';
import { UserService } from '../../../services/user.service';
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-mywebinarcertificate',
  templateUrl: './mywebinarcertificate.component.html',
  styleUrls: ['./mywebinarcertificate.component.css']
})
export class MywebinarcertificateComponent implements OnInit {

  env = environment;   
  pdfData:any;
  user:any;
  constructor(private userService: UserService, private authGuard: AuthGuard, private titleService: Title,private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.titleService.setTitle(environment.siteName + ' - My Webinar');	
      var webinar_id = this.activatedRoute.snapshot.paramMap.get('webinar_id');		
      var user_id = this.activatedRoute.snapshot.paramMap.get('user_id');		
  		this.userService.getCertificate(webinar_id,user_id).subscribe((response:any) => { 
        this.pdfData = response.data;
        console.log(this.pdfData);	  		  		
	    });
  }

}
