import { Component, OnInit } from '@angular/core';
import { Title,Meta }  from '@angular/platform-browser';
import { FormBuilder, FormGroup, FormControl, Validators,AbstractControl } from '@angular/forms';
import { HttpClient} from '@angular/common/http';
import { FlashMessagesService } from 'angular2-flash-messages';
import {Router, ActivatedRoute, Params} from '@angular/router';

import { UserService } from '../../../services/user.service';
import { environment } from '../../../../environments/environment';
import { AuthGuard } from '../../../services/authguard.service';





@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {

  	userForm: FormGroup; 
  	reset_token = ""; 
  	error ='';		

  	constructor(private userService: UserService,private http:HttpClient,private _flashMessagesService: FlashMessagesService,private router: Router,private titleService: Title, private activatedRoute: ActivatedRoute, private authGuard: AuthGuard, ) { }

  	ngOnInit() {  
  		this.reset_token = this.activatedRoute.snapshot.paramMap.get('reset_token');  
  		var str = this.userService.getUser(atob(this.reset_token)).subscribe(response => {
       		if(response["status"] == 'success' && response['data'] && response['data']._id == atob(this.reset_token))
       		{    
       			console.log('user found with this id '); 			       
  			}
  			else
  			{
  				this._flashMessagesService.show('Some internal error occured.', { cssClass: 'alert-success', timeout: 5000 });
  				 this.router.navigate(['/home']);  		
  			}	
    	});
  		this.titleService.setTitle(environment.siteName + ' - Reset Password');
 		  this.userForm = new FormGroup({
      		'password': new FormControl('',[
        		Validators.required,
        		Validators.minLength(6),        
      		]), 
      		'confirm_password': new FormControl('',[
        		Validators.required,
        		Validators.minLength(6),        		        
      		]),     		
    	},ValidateConfirmPassword);

  	}

  	public submitForm(){ 	
		this.userForm.value.userid = atob(this.reset_token); 
  		var str = this.userService.resetUserPassword(this.userForm.value).subscribe(response => {    
  		console.log(response);   		
       		if(response["status"] == 'success')
       		{
       			this._flashMessagesService.show('Password reset successfully. Please Login.', { cssClass: 'alert-success', timeout: 5000 });
  				this.router.navigate(['/home']);        
  			}
  			else
  			{
  				this.error = response["msg"];
  			}	
    	});
  	}

}




export function ValidateConfirmPassword(findForm: FormControl) {

	if (findForm["controls"].password.value == findForm["controls"].confirm_password.value) {	
		return null;
	}
	return { validConfirmPassword: true };
}





