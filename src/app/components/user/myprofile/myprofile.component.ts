import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Title,Meta }  from '@angular/platform-browser';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { FormBuilder, FormGroup, FormControl, Validators,AbstractControl } from '@angular/forms';
import { FlashMessagesService } from 'angular2-flash-messages';

import { AuthGuard } from '../../../services/authguard.service';
import { environment } from '../../../../environments/environment';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-myprofile',
  templateUrl: './myprofile.component.html',
  styleUrls: ['./myprofile.component.css']
})
export class MyprofileComponent implements OnInit {

    env = environment;
    user;
  	userForm: any = {};  	
    plandata: any = {}; 
    elpdata: any; 
  	constructor(private userService: UserService, private authGuard: AuthGuard , private router: Router, private activatedRoute: ActivatedRoute,private titleService: Title,private _flashMessagesService: FlashMessagesService) {
		
  	}

  	ngOnInit() {
  		this.titleService.setTitle(environment.siteName + ' - My Profile');	
  		this.user = this.authGuard.getUser();	  		
  		this.userService.getUser(this.user.user_id).subscribe((response:any) => { 
			  this.userForm = response.data;	
	    });	
      this.userService.getUserPlan(this.user.user_id).subscribe((response:any) => {
        this.plandata = response.data;     
	  });  
	  this.userService.getUserELPHistory(this.user.user_id).subscribe((response:any) => {
		this.elpdata = response.data;  
		
	  });  
  	}

  	public submitForm(){
  	  	this.userService.saveUser(this.userForm).subscribe(response => {       		
       	this._flashMessagesService.show('Profile Updated Successfully', { cssClass: 'alert-success', timeout: 5000 });
		   this.router.navigate(['/user/dashboard']);	
    	});
  	}

}
