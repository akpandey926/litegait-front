import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Title,Meta }  from '@angular/platform-browser';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { FormBuilder, FormGroup, FormControl, Validators,AbstractControl } from '@angular/forms';
import { FlashMessagesService } from 'angular2-flash-messages';

import { AuthGuard } from '../../../services/authguard.service';
import { environment } from '../../../../environments/environment';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']  
})
export class ChangepasswordComponent implements OnInit {

	env = environment;
  	user;
  	userForm: any = {};  	

  	constructor(private userService: UserService, private authGuard: AuthGuard , private router: Router, private activatedRoute: ActivatedRoute,private titleService: Title,private _flashMessagesService: FlashMessagesService) {
		
  	}

  	ngOnInit() {

  		this.titleService.setTitle(environment.siteName + ' - Change Password');

  		this.userForm = new FormGroup({
  			'id': new FormControl(''), 
  			'old_password': new FormControl('',[
        		Validators.required,
        		Validators.minLength(6),        
      		]),     		
      		'password': new FormControl('',[
        		Validators.required,
        		Validators.minLength(6),        
      		]), 
      		'confirm_password': new FormControl('',[
        		Validators.required,
        		Validators.minLength(6),        		        
      		]),     		
    	},ValidateConfirmPassword);	

  		this.user = this.authGuard.getUser();	  		
  			
  	}

  	public submitForm(){  		

  		if (this.userForm.valid) {
  			this.userForm.value.id = this.user.user_id;        	
        	this.userService.changePassword(this.userForm.value).subscribe(response => { 
        		if(response['status']=='success')      		
        		{
       				this._flashMessagesService.show('Password Changed Successfully', { cssClass: 'alert-success', timeout: 5000 });
  					this.router.navigate(['/user/dashboard']);	
  				}
  				else
  				{
  					this._flashMessagesService.show(response['msg'], { cssClass: 'alert-success', timeout: 5000 });
  				}	
    		});
        } else {
          this.validateAllFormFields(this.userForm);
        }
  	  		
  	  	
  	}

  	validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
          //console.log(field);
          const control = formGroup.get(field);
          if (control instanceof FormControl) {
            control.markAsTouched({ onlySelf: true });
          } else if (control instanceof FormGroup) {
            this.validateAllFormFields(control);
          }
        });
    }

}

export function ValidateConfirmPassword(findForm: FormControl) {

	if (findForm["controls"].password.value == findForm["controls"].confirm_password.value) {	
		return null;
	}
	return { validConfirmPassword: true };
}
