import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { environment } from '../../../environments/environment';
import { PartService } from '../../services/part.service';

@Component({
  selector: 'app-partlist',
  templateUrl: './partlist.component.html',
  styleUrls: ['./partlist.component.css']
})
export class PartlistComponent implements OnInit {

  env = environment;
  pageTitle = 'Parts';
  parts: any = [];
  categories: any = [];
  product: any = [];
  p = 1;
  itemsPerPage = 15;
  filterMetadata = { count: 0 };
  keyword: string = '';
  orderByType: boolean = false;
  category = "";
  is_product = "";
  type = "parts";
  show: boolean = false;

  constructor(private partService: PartService, private titleService: Title, private router: Router, private activatedRoute: ActivatedRoute) {
    // this.router.routeReuseStrategy.shouldReuseRoute = function () {
    //   return false;
    // }
  }

  ngOnInit() {
    var routeData = this.activatedRoute.snapshot.data;
    if (routeData['page'] == 'accessories') {
      this.type = 'accessories';
      this.pageTitle = 'Accessories List';
    }

    this.titleService.setTitle(environment.siteName + ' - ' + this.pageTitle);

    this.category = this.activatedRoute.snapshot.paramMap.get('category');
    this.is_product = this.activatedRoute.snapshot.paramMap.get('product');

    this.activatedRoute.params.subscribe(routeParams => {
      this.partService.getList(this.type, routeParams.category, routeParams.product).subscribe((response: any) => {
        this.parts = response.data;
        this.categories = response.categories;
        this.product = response.product;
      });
    });



  }



  onbtnClick(view: string) {
    // console.log(view); 
    if (view == 'list') {
      this.show = true;
    } else {
      this.show = false;
    }

  }





}
