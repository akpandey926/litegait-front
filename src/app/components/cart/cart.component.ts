import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { AuthGuard } from '../../services/authguard.service';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

import { environment } from '../../../environments/environment';
import { WebinarService } from '../../services/webinar.service';
import * as moment from 'moment-timezone';

import { Location } from '@angular/common';

@Component({
	selector: 'app-cart',
	templateUrl: './cart.component.html',
	styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

	cart: any = [];
	env = environment;
	user; 
	userTzTime:any;
	timeZone = moment.tz.guess();
	//timeZone = "America/new_york";
	// check_cart_prod:boolean = false; 

	constructor( private _flashMessagesService: FlashMessagesService, private authGuard: AuthGuard, private router: Router, private webinarService: WebinarService,  private activatedRoute: ActivatedRoute,private _location: Location) { }

	ngOnInit() {
		this.user = this.authGuard.getUser();
		this.cart = JSON.parse(localStorage.getItem("mycart"));
		/*
		if(this.cart && this.cart.items.length > 0){
          this.cart.items.forEach((message) => {
              // console.log(message.type); 
              if(message.type == "product" || message.type == "seminar" || message.type == "part"){
                this.check_cart_prod = true; 
              }
          });
        }else {
        	this.check_cart_prod = true; 
        }
        */
	}

	increaseqty(id) {
		var items = new Array();
		this.cart.items.forEach((value, key) => {
			if (value.id == id) {
				value.qty = value.qty + 1;
				value.total = value.price * value.qty;
				if (value.qty >= 1) {
					items.push(value);
				}
			} else {
				items.push(value);
			}
		});
		this.cart.items = items;
		
		this.update_cart();
	}

	getTiemzone(webinartime:any){
		var webinarDateTime1 = moment(webinartime);
		this.userTzTime = webinarDateTime1.tz(this.timeZone).format('z');
		return this.userTzTime;
	}

	decreaseqty(id) {
		var items = new Array();
		this.cart.items.forEach((value, key) => {
			if (value.id == id) {
				value.qty = value.qty - 1;
				value.total = value.price * value.qty;
				if (value.qty >= 1) {
					items.push(value);
				}
			} else {
				items.push(value);
			}
		});
		this.cart.items = items;
		this.update_cart();
	}

	deleteqty(id) {
		var items = new Array();
		this.cart.items.forEach((value, key) => {
			if (value.id != id) {
				items.push(value);
			}
		});
		this.cart.items = items;
		this.update_cart();
	}

	update_cart() {
		var items = this.cart.items;
		var total = 0;
		var totalqty = 0;
		this.cart.items.forEach((value, key) => {
			total = total + value.total;
			totalqty++;
		});
		this.cart.subtotal=total;
		this.cart.total = total;
		this.cart.totalqty = totalqty;

		localStorage.setItem("mycart", JSON.stringify(this.cart));

		var len = this.cart.items.length;
		if (len >= 1) {
			$('span.cartcnt').show().html(len);
		} else {
			$('span.cartcnt').hide();
		}
	}


	backClicked() {
		this._location.back();
	}


    purchase_webinar_directly(){ 
   		var check_free_webinar = true; 
          this.cart.items.forEach((message) => {
              if(message.type != "webinar" || ( message.type == "webinar" && ( this.cart.total > 0 || this.cart.subtotal > 0 ))){
                check_free_webinar = false; 
              }
          });
        if(check_free_webinar){
	    	var data:any={};
	    	data.user = this.user;
	    	data.cart = this.cart;
	    	data.usertime = this.timeZone;
	        this.webinarService.purchase_webinar_directly(data).subscribe((response:any) => { 
	            if (response["status"] == 'success') {
					this._flashMessagesService.show('Webinar purchased successfully.', { cssClass: 'alert-success', timeout: 5000 });
					$('span.cartcnt').hide();
					localStorage.removeItem('mycart');
  					localStorage.removeItem('checkout_address');
	                this.router.navigate(['/user/webinar']);
	            } else {
	               this._flashMessagesService.show(response["msg"], { cssClass: 'alert-danger', timeout: 5000 });
	            }            
	        });

        }else{
        	 this.router.navigate(['/checkout']);
        }
        // this.router.navigate(['/payment']);
    }





}
