import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { environment } from '../../../environments/environment';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-productlist',
  templateUrl: './productlist.component.html',
  styleUrls: ['./productlist.component.css']
})
export class ProductlistComponent implements OnInit {

  // env = environment;
  // products:any = [];
  //  category:any = [];
  //  banner_image='';
  // p = 1;
  //  itemsPerPage = 20;
  //  filterMetadata = { count: 0 };
  //  keyword: string = '';
  //  orderByType:boolean = false;


  env = environment;
  pageTitle = 'Part List';
  parts: any = [];
  products: any = [];
  banner_image = '';
  categories: any = [];
  product: any = [];
  p = 1;
  itemsPerPage = 15;
  filterMetadata = { count: 0 };
  keyword: string = '';
  orderByType: boolean = false;
  // category = "";
  category: any = [];
  is_product = "";
  type = "parts";
  show: boolean = false;







  constructor(private productService: ProductService, private titleService: Title, private router: Router, private activatedRoute: ActivatedRoute,private chRef: ChangeDetectorRef) {

    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    }

  }

  ngOnInit() {
    
    this.titleService.setTitle(environment.siteName + ' - Product List');
    this.activatedRoute.params.subscribe(routeParams => {
        var slug = this.activatedRoute.snapshot.paramMap.get('slug');
        this.productService.getList(slug).subscribe((response: any) => {

          this.products = response.data;

          this.category = response.category;

          this.banner_image = this.category.banner_image;
          this.chRef.detectChanges();

          console.log("response",response)

          if (response.data && response.data[0]) {

            console.log("responseTRUE",response)

            this.router.navigate(['/product/' + response.data[0].slug]);
          }
        });
    });



  }

  typeOf(str) {
    return typeof str;
  }



  onbtnClick(view: string) {

    if (view == 'list') {
      this.show = true;
    } else {
      this.show = false;
    }

  }



}
