import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';

import { CommonService } from '../../../services/common.service';

@Component({
  selector: 'app-elp',
  templateUrl: './elp.component.html',
  styleUrls: ['./elp.component.css']
})
export class ElpComponent implements OnInit {
	
	constructor(private router: Router,private commonService: CommonService) { }
	
	elps = null;
	
	ngOnInit() {
		this.commonService.getElpList().subscribe((response:any) => { 
  			this.elps = response.data;	 			
        });
	}
	
	goToPayment(id,price) : void{
		this.router.navigate(['/elp-payment/'+id+'/'+price]);
	}
}
