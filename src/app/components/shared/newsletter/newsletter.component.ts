import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';

import { HomeService } from '../../../services/home.service';
import { environment } from '../../../../environments/environment';
declare var hbspt:any;
@Component({
    selector: 'app-newsletter',
    templateUrl: './newsletter.component.html',
    styleUrls: ['./newsletter.component.css']
})
export class NewsletterComponent implements OnInit {

    newsletterForm: FormGroup;

    constructor(private homeService: HomeService) { }

    ngOnInit() {
        this.newsletterForm = new FormGroup({
            'email': new FormControl('', [
                Validators.required,
                Validators.email,
            ]),
        });
    }


    newsletter_subscribe() {

        this.homeService.newsletterSubscribe(this.newsletterForm.value).subscribe(response => {
            if (response['status'] == 'success') {
                this.newsletterForm.value.email = '';
                alert('Thank you for subscribe with us!');
            }
        });
    }
    ngAfterViewInit(){
        setTimeout(function () {
            var ele =document.getElementById('hubspotForm');	  		
            if(ele != null)
            {
                var portalId = ele.getAttribute("portalId");
                var formId = ele.getAttribute("formId");

                hbspt.forms.create({
                  portalId: portalId,
                  formId: formId,
                  target: "#hubspotForm"
                });
            }

      }, 100);  		
    }

}
