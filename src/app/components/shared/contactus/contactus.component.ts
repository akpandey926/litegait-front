import { Component, OnInit, Input } from '@angular/core';
import { Title,Meta }  from '@angular/platform-browser';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { HttpClient} from '@angular/common/http';
import { FlashMessagesService } from 'angular2-flash-messages';

import { environment } from '../../../../environments/environment';
import { CommonService } from '../../../services/common.service';


@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class ContactusComponent implements OnInit {
	
	distributorData: any = [];
	regionData: any = [];
 	userForm: FormGroup;
	env = environment;	
	public res:any;	
   	error = '';
   	@Input() slug: string;

  	constructor(private router: Router, private commonService: CommonService,  private http:HttpClient,  private _flashMessagesService: FlashMessagesService) { }

  	ngOnInit() {

		this.userForm = new FormGroup({
		  'first_name': new FormControl('', [
		    Validators.required,
		  ]),
		  'last_name': new FormControl('', [
		    Validators.required,
		  ]),
		  'email': new FormControl('', [
		    Validators.required,
		    Validators.email,
		  ]),
		  'address': new FormControl('', [
		    // Validators.required,
		  ]),
		  'message': new FormControl('', [
		    Validators.required,
		  ]),
		});
		
		this.commonService.getDistributorList().subscribe((response: any) => {
			this.distributorData = response.data;
			this.regionData = response.regionData;
		});
  	}

    public sendContactEmail() {
      	var str = this.commonService.sendContactEmail(this.userForm.value).subscribe(response => {
       		if (response["status"] == 'success') {
            	this._flashMessagesService.show('Contact Request Sent Successfully. We will get back to you very soon!', { cssClass: 'alert-success', timeout: 5000 });
            	this.userForm.reset();
        	}
        	else {          
          		this._flashMessagesService.show('Error! Request not sent.', { cssClass: 'alert-danger', timeout: 5000 });
        	}
      });

    }




}
