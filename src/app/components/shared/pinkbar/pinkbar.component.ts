import { Component, OnInit,Input } from '@angular/core';

import { CommonService } from '../../../services/common.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-pinkbar',
  templateUrl: './pinkbar.component.html',
  styleUrls: ['./pinkbar.component.css']
})
export class PinkbarComponent implements OnInit {

	@Input()
  	page = '';
  	@Input()
  	banner_image = '';

	env = environment;
  	constructor(private commonService: CommonService) { }

  	ngOnInit() {  		
  	}

  	getBanner(page)
  	{
  		if(this.banner_image)
  		{
  			return this.env.serverUrl+this.env.galleryDir+this.banner_image;
  		}

  		if(this.commonService.pageBanner)
  		{
  			var banner = this.commonService.pageBanner[this.page]; 
	        if(banner)
	        {
	  			  return this.env.serverUrl+this.env.galleryDir+banner;
	        }  
  		}

      	return '';

  		//var banner = JSON.parse(localStorage.getItem('pageBanner'))[this.page];
  		//return this.env.serverUrl+this.env.galleryDir+banner;	
  	}

}
