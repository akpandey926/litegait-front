import { Component, OnInit, Input } from '@angular/core';
import * as $ from 'jquery';

import { environment } from '../../../../environments/environment';
import { CommonService } from '../../../services/common.service';

@Component({
    selector: 'app-pagetestimonials',
    templateUrl: './pagetestimonials.component.html',
    styleUrls: ['./pagetestimonials.component.css']
})
export class PagetestimonialsComponent implements OnInit {

    @Input()
    page = '';
    @Input()
    id = '';

    env = environment;
    testimonials: any = [];

    constructor(private commonService: CommonService) { }

    ngOnInit() {
        /* console.log(this.page);
        console.log(this.id); */
        this.commonService.getPageTestimonials(this.page, this.id).subscribe((response: any) => {
            this.testimonials = response.data;
        });

    }

    ngAfterViewInit() {
        $(document).ready(function () {

            setTimeout(function () {

                (function ($) {

                    (<any>$('.newslist')).owlCarousel({
                        autoplay: true,
                        autoplayTimeout: 1000,
                        slideSpeed: 800,
                        autoplayHoverPause: true,
                        nav: true,
                        dots: true,
                        navigation: true,
                        pagination: true,
                        // navigationText:["<img src='assets/images/top_slider_left.png' alt='' />","<img src='assets/images/top_slider_right.png' alt='' />"],
                        items: 1,
                        margin: 0,
                        loop: true,
                        itemsDesktop: [1199, 1],
                        itemsDesktopSmall: [980, 1],
                        itemsTablet: [768, 1],
                        itemsMobile: [479, 1],
                    });

                })(jQuery);

            }, 5000);
        });
    }

}
