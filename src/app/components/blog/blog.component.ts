import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';

import { CommonService } from '../../services/common.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {

  	env = environment;
  	blogData:any = [];
  	slug="";
  	
  	constructor(private commonService: CommonService, private activatedRoute: ActivatedRoute ) {}

  	ngOnInit() {

  		this.slug = this.activatedRoute.snapshot.paramMap.get('slug');

  		if(this.slug != null)
  		{
  			this.commonService.getBlogView(this.slug).subscribe((response:any) => {
  				this.blogData = response.data;       		
    		});
  		}
  		else
  		{
  			this.commonService.getBlogList().subscribe((response:any) => {
  				this.blogData = response.data;       		
    		});
    	}	

  	}

}
