import { Component, OnInit } from '@angular/core';
import { Title,Meta }  from '@angular/platform-browser';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

import { environment } from '../../../environments/environment';
import { WebinarService } from '../../services/webinar.service';
import { AuthGuard } from '../../services/authguard.service';

@Component({
  selector: 'app-seminar',
  templateUrl: './seminar.component.html',
  styleUrls: ['./seminar.component.css']
})
export class SeminarComponent implements OnInit {

	env = environment;
  	seminar:any = [];     
    user; 
    check_plan;   
    data;   
    error = '';	
    seminarTopText:any = '';

  	constructor( private _flashMessagesService: FlashMessagesService, private webinarService: WebinarService,  private authGuard: AuthGuard, private titleService: Title, private meta: Meta, private router: Router, private activatedRoute: ActivatedRoute) { 
  	}

  	ngOnInit() {
               
        var slug = this.activatedRoute.snapshot.paramMap.get('slug');
        this.webinarService.getSeminarView(slug).subscribe((response:any) => { 
    		this.seminar = response.data;
    		this.seminarTopText = response.seminarTopText;
          	if(response.data.meta_title)
    		  this.titleService.setTitle(environment.siteName + ' - '+response.data.meta_title);	
          	if(response.data.meta_description) 		
            	this.meta.addTag({ name: 'description', content: response.data.meta_description });  	
        });

        this.user = this.authGuard.getUser();            
        /* if(this.user.email && new Date(this.user.elp_expire).getTime() > new Date().getTime()){
          	this.check_plan = 1; 
        }else{
          	this.check_plan = ''; 
        } */

  	}

    purchase_by_plan(){
      if(this.user.elp_plan){
        this.data = {}; 
        this.data.user_id = this.user.user_id; 
        this.data.seminar_id = this.seminar._id; 
        this.webinarService.purchaseSeminarByPlan(this.data).subscribe((response:any) => { 
            if (response["status"] == 'success') {
                this._flashMessagesService.show('Seminar purchased successfully.', { cssClass: 'alert-success', timeout: 5000 });
                this.router.navigate(['/user/seminar']);
            } else {
               this.error = response["msg"];
                 this._flashMessagesService.show(response["msg"], { cssClass: 'alert-success', timeout: 5000 });
            }            
        });
      }
    }



}
