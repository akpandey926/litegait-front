import { Component, OnInit } from '@angular/core';
import { Title,Meta }  from '@angular/platform-browser';
import {Router, ActivatedRoute, Params} from '@angular/router';

import { environment } from '../../../environments/environment';
import { WebinarService } from '../../services/webinar.service';

@Component({
  selector: 'app-seminarlist',
  templateUrl: './seminarlist.component.html',
  styleUrls: ['./seminarlist.component.css']
})
export class SeminarlistComponent implements OnInit {

	env = environment;
  	seminars:any = [];    
  	p = 1;
    itemsPerPage = 20;
    filterMetadata = { count: 0 };
    keyword: string = '';
    orderByType:boolean = false;
    seminarTopText:any = '';


  	constructor(private webinarService: WebinarService, private titleService: Title,private router: Router, private activatedRoute: ActivatedRoute) { 
  	}

  	ngOnInit() {
                
  		this.titleService.setTitle(environment.siteName + ' - Webinars');	

        this.webinarService.getSeminarList().subscribe((response:any) => { 
  			this.seminars = response.data;  			
  			this.seminarTopText = response.seminarTopText;  			
        });   		  
  	}

}
