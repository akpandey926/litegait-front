import { Component, OnInit } from '@angular/core';
import { Title,Meta }  from '@angular/platform-browser';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { HttpClient} from '@angular/common/http';

import { environment } from '../../../environments/environment';
import { AuthGuard } from '../../services/authguard.service';
import { CommonService } from '../../services/common.service';
import { FlashMessagesService } from 'angular2-flash-messages';

declare var hbspt:any;

@Component({
  selector: 'app-staticpage',
  templateUrl: './staticpage.component.html',
  styleUrls: ['./staticpage.component.css']
})
export class StaticpageComponent implements OnInit {
	
  userForm: FormGroup;

	env = environment;	
	public res:any;
	banner_image='';
  	page_video:string = '' ; 
   	error = '';
   	slug: any = ''; 
	
  	constructor(private router: Router, private commonService: CommonService,  private http:HttpClient, private activatedRoute: ActivatedRoute, private titleService: Title,private meta: Meta, private authGuard: AuthGuard, private _flashMessagesService: FlashMessagesService) { 

  		this.router.routeReuseStrategy.shouldReuseRoute = function(){
        	return false;
     	}
  	}

  	ngOnInit() {	

  		this.slug = this.activatedRoute.snapshot.paramMap.get('slug');
  		var languageCode = this.authGuard.getUserLanguageCode();

        this.http.post(environment.apiUrl+"/common/page/"+this.slug,{'languageCode':languageCode}).subscribe((response) => {      
			this.res = response;
			// console.log(this.res.data); 
			this.page_video = this.res.data.page_video; 
			this.banner_image = this.res.data.banner_image;        
			this.titleService.setTitle(environment.siteName + ' - ' + this.res.data.meta_title);  
			this.meta.addTag({ name: 'description', content: this.res.data.meta_description });                    
        });

  	}  

  	ngAfterViewInit(){
  		setTimeout(function () {
  			var ele =document.getElementById('hubspotForm');	  		
	  		if(ele != null)
	  		{
	  			var portalId = ele.getAttribute("portalId");
	  			var formId = ele.getAttribute("formId");

	  			hbspt.forms.create({
					portalId: portalId,
					formId: formId,
					target: "#hubspotForm"
	  			});
	  		}

        }, 100);  		
  	}

}
