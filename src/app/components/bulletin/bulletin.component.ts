import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import * as $ from 'jquery';

import { CommonService } from '../../services/common.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-bulletin',
  templateUrl: './bulletin.component.html',
  styleUrls: ['./bulletin.component.css']
})
export class BulletinComponent implements OnInit {

	env = environment;
	bulletinData: any = [];  	
  	p = 1;
    itemsPerPage = 40; 
    year:any='';


  	constructor(private commonService: CommonService, private router: Router, private activatedRoute: ActivatedRoute) {  		
  	}

  	ngOnInit() {

  		this.commonService.getBulletinList().subscribe((response: any) => {
			this.bulletinData = response.data;
			this.setYear();			
	    });
  	}

  	getYear(bulletin){
  		var date = new Date(bulletin.publish_date);
  		var yr = date.getFullYear();
  		
  		if(yr!=this.year)
  		{	
  			this.year = yr;
  			return true;
  		}
  		else
			return false;	
  	}

  	ngAfterViewInit() {
        
    }

    setYear(){

    	$(document).ready(function () {

            setTimeout(function () {

                (function ($) {

                	(<any>$(".monthheading")).each(function( index ) {
  						var rel = $( this ).attr('rel');
  						$('.monthheading[rel="'+rel+'"]').hide();
  						$('.monthheading[rel="'+rel+'"]:first').show();
					});
                    
                    // (<any>$('.cloud-zoom')).CloudZoom();

                })(jQuery);

            }, 500);
        });

    }


}
