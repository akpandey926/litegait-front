import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { environment } from '../../../environments/environment';
import { WebinarService } from '../../services/webinar.service';

@Component({
    selector: 'app-instructorlist',
    templateUrl: './instructorlist.component.html',
    styleUrls: ['./instructorlist.component.css']
})
export class InstructorlistComponent implements OnInit {

    env = environment;
    instructors: any = [];
    p = 1;
    itemsPerPage = 30;
    filterMetadata = { count: 0 };
    keyword: string = '';
    orderByType: boolean = false;

    constructor(private webinarService: WebinarService, private titleService: Title, private router: Router, private activatedRoute: ActivatedRoute) {
    }

    ngOnInit() {

        this.titleService.setTitle(environment.siteName + ' - Instructors & Trainers');

        this.webinarService.getInstructorList().subscribe((response: any) => {
            this.instructors = response.data;
        });
    }

}
