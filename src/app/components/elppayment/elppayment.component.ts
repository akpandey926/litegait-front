import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Title,Meta }  from '@angular/platform-browser';
import { FormBuilder, FormGroup, FormControl, Validators,AbstractControl } from '@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { PayPalConfig, PayPalEnvironment, PayPalIntegrationType } from 'ngx-paypal';

import { AuthGuard } from '../../services/authguard.service';
import { CommonService } from '../../services/common.service';
import { environment } from '../../../environments/environment';
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-elppayment',
  templateUrl: './elppayment.component.html',
  styleUrls: ['./elppayment.component.css']
})
export class ElppaymentComponent implements OnInit {
	
	payPalConfig;
	user;
	elp;
	price;
	userForm: FormGroup; 
 	show:boolean = false;  
 	buttonName:any = 'Do you have coupon?';
 	error = ""; 
 	couponSuccessMsg = ""; 
 	couponErrorMsg = "" ; 
 	showFacility: boolean = false; 
 	showFacilityError: boolean = false; 
	facilityName = ''; 
	timeZone = moment.tz.guess();
	@ViewChild('facility_name') facility_name: ElementRef; 


	constructor(private commonService: CommonService,private authGuard: AuthGuard, private titleService: Title, private router: Router,private activatedRoute: ActivatedRoute, private _flashMessagesService: FlashMessagesService) { }




	ngOnInit() {
		this.user = this.authGuard.getUser();
 		this.titleService.setTitle(environment.siteName + ' - Elp-Payment');
		
		var elp_id = this.activatedRoute.snapshot.paramMap.get('elp_id');
		this.price = this.activatedRoute.snapshot.paramMap.get('price');
		
		this.commonService.getElpView(elp_id).subscribe((response:any) => { 
  			this.elp = response.data;

		this.show =   this.elp.persons > 1 ? false : true; 

  			// console.log(this.elp); 						
        });
        this.initConfig();


  		this.userForm = new FormGroup({
      		'coupon_code': new FormControl('',[
        		Validators.required,      		
      		])    		
    	});


	}


	
	private initConfig(): void {

     	this.payPalConfig = new PayPalConfig(PayPalIntegrationType.ClientSideREST, PayPalEnvironment.Sandbox, {
	        commit: true,
	        client: {
			  sandbox: 'AWQyGULqIz7CqYioh79Z2DekwFO_sWAfYiyPaMD3UYOZ2nlHKfvDI-mzocXdqDbpBW4XQpe28C2ju3ay'
			  //production: 'AVYWMUjUgMRsvbZKLG0uTvPBE6cYrouwivD_bpOVTPjgq-UXHwQ-9hLz2c6_3hNRqSTm5BEIBcEQ-0YO' //PayPalEnvironment.Production
	        },
	        button: {
	          label: 'pay',
	          size: 'large',
	          color: 'gold',
	          shape :'rect'
	        },	       
	        onPaymentComplete: (data, actions) => {	        	
	          	this.submitPayment(data);
	        },
	        onCancel: (data, actions) => {
	          	this._flashMessagesService.show('Error in Payment cancel!, Please try Again.', { cssClass: 'alert-danger', timeout: 5000 });
	          	this.router.navigate(['/elp-payment/'+this.elp._id+'/'+this.elp.price]);
	        },
	        onError: (err) => {
	          	this._flashMessagesService.show('Error in Payment!, Please try Again.', { cssClass: 'alert-danger', timeout: 5000 });
	          	this.router.navigate(['/elp-payment/'+this.elp._id+'/'+this.elp.price]);
	        },
	        transactions: [{
	          amount: {
	            currency: 'USD',
	            total: this.price
	          }
	        }]
      	});
    }
	
	private submitPayment(paymentData){		

		var data:any={};		
		data.payment = {'payment_method':'paypal','payment_id':paymentData.paymentID};
		data.user = this.user;
		data.elp_id = this.elp._id;		 
		data.usertime = this.timeZone;
		data.facility_name = this.facility_name ? this.facility_name.nativeElement.value : ''; 		
		this.commonService.saveElpPayment(data).subscribe((response:any) => { 						
			this._flashMessagesService.show('Thank you for Purchasing this plan. Your New plan is active Now.', { cssClass: 'alert-success', timeout: 5000 });
  			this.router.navigate(['/user/myprofile']);
		});
	}




  	public submitForm(){ 	
  		this.userForm.value.user = this.user; 
  		this.userForm.value.usertime = this.timeZone; 
  		// console.log(this.userForm.value); 
  		// return;    
  		var str = this.commonService.saveElpByCoupon(this.userForm.value).subscribe(response => {       		
       		if(response["status"] == 'success')
       		{
       			    			
  				this.couponErrorMsg = ''; 
  				this.couponSuccessMsg = response["msg"];   
  				this._flashMessagesService.show('Thank you for Purchasing this plan. Your New plan is active Now.', { cssClass: 'alert-success', timeout: 5000 });
  				this.router.navigate(['/user/myprofile']);   
  			}
  			else
  			{
  				this.couponSuccessMsg = ''; 
  				this.couponErrorMsg = response["msg"];



  			}	
    	});
    	 
  	}

  	public checkValue(val){
  		this.facilityName = val; 

  		if(val && val.length > 0){
  			this.showFacility = true; 
  			this.showFacilityError = false; 
  		}else {
  			this.showFacility = false; 
  			this.showFacilityError = true; 

  		}
  	}
}
