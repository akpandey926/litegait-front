import { Component, OnInit } from '@angular/core';
import { Title,Meta }  from '@angular/platform-browser';
import { FormBuilder, FormGroup, FormControl, Validators,AbstractControl } from '@angular/forms';
import { HttpClient} from '@angular/common/http';
import { FlashMessagesService } from 'angular2-flash-messages';
import {Router} from "@angular/router"

import { UserService } from '../../services/user.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {	

  	userForm: FormGroup; 

  	error ='';		

  	constructor(private userService: UserService,private http:HttpClient,private _flashMessagesService: FlashMessagesService,private router: Router,private titleService: Title) { }

  	ngOnInit() {  
  		this.titleService.setTitle(environment.siteName + ' - Register');

  		this.userForm = new FormGroup({
      		'name': new FormControl('', [
        		Validators.required,
        		Validators.minLength(4),        
			]),
			'captcha': new FormControl('',[Validators.required]),
      		'email': new FormControl('',[
        		Validators.required,
        		Validators.email,         		
      		],this.isEmailUnique.bind(this)),
      		'phone': new FormControl('',[
        		Validators.minLength(10),        
      		]),
      		'password': new FormControl('',[
        		Validators.required,
        		Validators.minLength(6),        
      		]), 
      		'confirm_password': new FormControl('',[
        		Validators.required,
        		Validators.minLength(6),        		        
      		]),     		
    	},ValidateConfirmPassword);
  	}

	//function to resolve the reCaptcha and retrieve a token
	async resolved(captchaResponse: string) {
		console.log(`Resolved response token: ${captchaResponse}`);
		await this.sendTokenToBackend(captchaResponse); //declaring the token send function with a token parameter
	}

	//function to send the token to the node server
	sendTokenToBackend(tok){
		//calling the service and passing the token to the service
		this.userService.sendToken(tok).subscribe(
			data => { console.log("true block");
			console.log(data)
			},
			err => {  console.log("false block");
			console.log(err)
			},
			() => {}
		);
	}




  	public submitForm(){ 	

  		var str = this.userService.addUser(this.userForm.value).subscribe(response => {       		
       		if(response["status"] == 'success')
       		{
       			this._flashMessagesService.show('Thank you for your Registration! Your Account is created Successfully, Now you can login on site using your email and password.', { cssClass: 'alert-success', timeout: 5000 });
  				  this.router.navigate(['/login']);        
  			}
  			else
  			{
  				this.error = response["msg"];
  			}	
    	});
    	 
  	}

  	isEmailUnique(control: FormControl) {  		

  		return this.http.get(environment.apiUrl+'/users/check_email/'+control.value);
  	}	

}

export function ValidateConfirmPassword(findForm: FormControl) {

	if (findForm["controls"].password.value == findForm["controls"].confirm_password.value) {	
		return null;
	}
	return { validConfirmPassword: true };
}