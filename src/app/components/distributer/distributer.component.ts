import { Component, OnInit } from '@angular/core';
import { Title }  from '@angular/platform-browser';
import { CommonService } from '../../services/common.service';
import { environment } from '../../../environments/environment';


@Component({
  selector: 'app-distributer',
  templateUrl: './distributer.component.html',
  styleUrls: ['./distributer.component.css']
})
export class DistributerComponent implements OnInit {
  distributersLists :any;
  constructor(private commonService: CommonService,private titleService: Title) { }

  ngOnInit() {
    this.titleService.setTitle(environment.siteName + ' - Distributers');
    this.commonService.getDistributers().subscribe((response: any) => {
      this.distributersLists = response.data;
    });
  }

}
