import { Component, OnInit,ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { FlashMessagesService } from 'angular2-flash-messages';
import { AuthGuard } from '../../services/authguard.service';
import { environment } from '../../../environments/environment';
import { ProductService } from '../../services/product.service';
declare var hbspt: any;
@Component({
  selector: 'app-requestquote',
  templateUrl: './requestquote.component.html',
  styleUrls: ['./requestquote.component.css']
})
export class RequestquoteComponent implements OnInit {

  env = environment;
  quoteFormData: any = [];
  product: any = [];
  quoteForm: any = {};
  quoteFormCustom: any = {};
  error: '';
  type = "";
  slug = "";
  user: any;
  categoryName: any;

  anyProduct: any;
  hugNgoProduct: any;
  tredmillProduct: any;
  partProducts: any;
  homeProducts: any;
  constructor(private productService: ProductService, private router: Router, private activatedRoute: ActivatedRoute, private titleService: Title, private _flashMessagesService: FlashMessagesService, private authGuard: AuthGuard,private chRef: ChangeDetectorRef) {

  }

  ngOnInit() {
    this.type = this.activatedRoute.snapshot.paramMap.get('type');
    this.slug = this.activatedRoute.snapshot.paramMap.get('slug');



    this.productService.getProductQuoteForm(this.type, this.slug).subscribe((response: any) => {
      this.quoteFormData = response.data;

      this.product = response.product;
      this.productService.getCategoryName(this.product.category).subscribe((response: any) => {
        this.categoryName = response.data.slug;
        

       
        if (this.type == "product" && this.categoryName == 'hugn-go') {
          this.hugNgoProduct = true;
        } else if (this.type == "product" && this.categoryName == 'treadmills') {
          this.tredmillProduct = true;
        } else if (this.type == "product" && this.categoryName == 'litegait-4-home') {

          this.homeProducts = true;
        } else if (this.type == "part") {
          this.partProducts = true;
        } else {
          this.anyProduct = true;
        }
        this.chRef.detectChanges();
        var ele = document.getElementById('hubspotForm');
          console.log(ele);
          if (ele != null) {
            var portalId = ele.getAttribute("portalId");
            var formId = ele.getAttribute("formId");
    
            hbspt.forms.create({
              portalId: portalId,
              formId: formId,
              target: "#hubspotForm"
            });
          }

      });
      this.titleService.setTitle(environment.siteName + ' - ' + response.product.name + ' - Request A Quote');
    });


  }

  public submitForm() {
    this.user = this.authGuard.getUser();
    this.quoteForm.product_id = this.product._id;
    this.quoteForm.type = this.type;
    this.quoteForm.address = this.user.address;
    this.quoteForm.itemName = this.product.name;
    this.quoteForm.customData = this.quoteFormCustom;
    var str = this.productService.saveProductQuoteForm(this.quoteForm).subscribe(response => {
      if (response["status"] == 'success') {
        this._flashMessagesService.show('Thank you for your Inquiry! Our customer service representatives will email you with a quote as soon as possible.', { cssClass: 'alert-success', timeout: 5000 });
        if (this.type == 'product')
          this.router.navigate(['/product/' + this.product.slug]);
        else if (this.type == 'part')
          this.router.navigate(['/part/' + this.product.slug]);
      }
      else {
        this.error = response["msg"];
      }
    });
  }

  stringtoArray(str) {
    if (str == undefined)
      return new Array();

    var array = str.split(';');
    return array;
  }
  ngAfterViewInit() {
    setTimeout(function () {
     

    }, 1000);
  }
}
