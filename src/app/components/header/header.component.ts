import { Component, OnInit, ViewChild, ElementRef, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
/* import * as $ from 'jquery'; */
import { Title, Meta } from '@angular/platform-browser';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { FlashMessagesService } from 'angular2-flash-messages';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { AuthGuard } from '../../services/authguard.service';
import { environment } from '../../../environments/environment';
import { CommonService } from '../../services/common.service';
import { UserService } from '../../services/user.service';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

	user;
	env = environment;
	mainMenuData: any = [];
	languageData: any = [];
	regionData: any = [];
	modalRef: BsModalRef;
	userForm: FormGroup;
	error = '';
	@ViewChild('loginPopupTemplate') loginPopupTemplate: ElementRef;

	constructor(private modalService: BsModalService, private authGuard: AuthGuard, private commonService: CommonService, private router: Router, private activatedRoute: ActivatedRoute, private userService: UserService, private _flashMessagesService: FlashMessagesService) { }

	ngOnInit() {

		this.user = this.authGuard.getUser();
		this.authGuard.change.subscribe(value => {
			this.user = this.authGuard.getUser();
		});

		var regionCode = this.activatedRoute.snapshot.paramMap.get('regionCode');
		if (regionCode != null) {
			this.authGuard.changeRegion(regionCode);
			this.router.navigate(['/home']);
		}

		var languageCode = this.activatedRoute.snapshot.paramMap.get('languageCode');
		var languageName = this.activatedRoute.snapshot.paramMap.get('languageName');
		if (languageCode != null) {
			this.authGuard.changeLanguage(languageCode, languageName);
			this.router.navigate(['/home']);
		}

		this.commonService.getLanguageList().subscribe((response: any) => {
			this.languageData = response.data;
		});

		this.commonService.getRegionList().subscribe((response: any) => {
			this.regionData = response.data;
		});

		this.commonService.getMenuList('main').subscribe((response: any) => {
			this.mainMenuData = response.data;
		});

		/* $(document).ready(function(){
			alert("hi");
		}); */

		var routeData = this.activatedRoute.snapshot.data;
		if (routeData['page'] == 'logout') {
			this.logOut();
		}

		this.userForm = new FormGroup({
			'email': new FormControl('', [
				Validators.required,
				Validators.email,
			]),
			'password': new FormControl('', [
				Validators.required,
				Validators.minLength(6),
			]),
		});

	}

	sizeOf(obj) {
		return Object.keys(obj).length;
	}

	openLoginModal() {
		this.modalRef = this.modalService.show(this.loginPopupTemplate);
	}

	closeLoginModal(template: TemplateRef<any>) {
		this.modalRef.hide();
	}

	public submitForm() {

		var str = this.userService.loginUser(this.userForm.value).subscribe(response => {
			if (response["status"] == 'success') {
				this.authGuard.setUser(response["data"]);
				this.modalRef.hide();
				this.router.navigate(['/user/dashboard']);
			} else {
				this.error = response["msg"];
			}
		});
	}

	public logOut() {
		this.authGuard.deleteUser();
		this._flashMessagesService.show('Logout Successfull.', { cssClass: 'alert-success', timeout: 5000 });
		this.router.navigate(['/home']);
	}

	disableSort(){
		return 0;
	}

}
