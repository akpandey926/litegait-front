import { Component, OnInit } from '@angular/core';
import { Title, Meta, DomSanitizer}  from '@angular/platform-browser';
import * as $ from 'jquery';
import { ActivatedRoute } from '@angular/router';

import { environment } from '../../../environments/environment';
import { CategoryService } from '../../services/category.service';

@Component({
  selector: 'app-categorylist',
  templateUrl: './categorylist.component.html',
  styleUrls: ['./categorylist.component.css']
})
export class CategorylistComponent implements OnInit {

	env = environment;
  	categories:any = [];  	    
  	products:any = [];  
    p = 1;
    p2 = 1;
    itemsPerPage = 18;
    itemsPerPage2 = 5;
    filterMetadata = { count: 0 };
    orderByType = false; 
    show: boolean = false; 

  	constructor(private categoryService: CategoryService, private titleService: Title, private activatedRoute: ActivatedRoute, private sanitizer: DomSanitizer) { }


  	ngOnInit() {

  		this.titleService.setTitle(environment.siteName + ' - Product List');	

  		this.categoryService.getList('product').subscribe((response:any) => { 
        this.categories = response.data;   	
  			this.products = response.products;   			 			
    	});
  	}

  // 	ngAfterViewInit() {
 	// 	$(document).ready(function(){

 	// 		setTimeout(function(){
      			
  //     			(function ($) {

		// 		    (<any>$('.item-hover-class')).hide();
		// 			(<any>$('.item-block')).mouseenter(function() {
		// 				$(this).find('.item-hover-class').addClass('overlay-block');
		// 				$(this).find('.item-hover-class').show();
		// 			}).mouseleave(function() {
		// 				$(this).find('.item-hover-class').removeClass('overlay-block');
		// 				$(this).find('.item-hover-class').hide();
		// 			});
				     

  //       		})(jQuery);

  //   		},100); 			 			
  // 		}); 
 	// }

    onbtnClick(view: string){
      // console.log(view); 
        if(view == 'list'){
            this.show = true; 
        }else {
            this.show = false; 
        }
    }
    truncateText(source, size) {
      var regex = /(<([^>]+)>)/ig
    ,body = source
    ,result = body.replace(regex, "");
      return result.length > size ? result.slice(0, size - 1) + "…" : result;
      }
  onChange(deviceValue) {
      if(deviceValue == 'asc'){
        this.orderByType = false; 
      }else if(deviceValue == 'desc') {
        this.orderByType = true; 
      }
  }
  overlay_show(obj){
    obj.currentTarget.children[1].style.display = 'flex';
  }
  overlay_hide(obj){
    obj.currentTarget.children[1].style.display = 'none';
  }

}
