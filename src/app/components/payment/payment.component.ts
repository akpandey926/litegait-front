import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { PayPalConfig, PayPalEnvironment, PayPalIntegrationType } from 'ngx-paypal';

import { AuthGuard } from '../../services/authguard.service';
import { environment } from '../../../environments/environment';
import { OrderService } from '../../services/order.service';
import * as moment from 'moment-timezone';

@Component({
	selector: 'app-payment',
	templateUrl: './payment.component.html',
	styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

	payPalConfig;
	cart: any = [];
	env = environment;
	user;
	items: any = [];
	timeZone = moment.tz.guess();
	constructor(private authGuard: AuthGuard, private orderService: OrderService, private titleService: Title, private router: Router, private activatedRoute: ActivatedRoute, private _flashMessagesService: FlashMessagesService) { }

	ngOnInit() {

		this.user = this.authGuard.getUser();
		this.titleService.setTitle(environment.siteName + ' - Checkout');
		this.cart = JSON.parse(localStorage.getItem("mycart"));

		// New array for item description
		this.cart.items.forEach((value, key) => {
			//items.name = value.name;
			//value.price = value.price; 
			//value.quantity = value.qty; 
			this.items.push({ 'name': value.name, 'description': value.name, 'price': value.price, 'quantity': value.qty });
			
		});
		localStorage.setItem("item_lists", JSON.stringify(this.items));
		

		this.initConfig();

		if (this.cart.total == 0) {
			var data = { 'paymentID': 'Not Required' };
			this.submitOrder(data);
		}

		if (localStorage.getItem('login_as_admin') == '1') {
			var data = { 'paymentID': 'Not Required' };
			this.submitOrder(data);
		}

	}

	private initConfig(): void {
		var getItemList = JSON.parse(localStorage.getItem("item_lists"));
		var countryCode = JSON.parse(localStorage.getItem('countryCode'));
		var stateCode = JSON.parse(localStorage.getItem('stateCode'));
		var checkoutAddress = JSON.parse(localStorage.getItem("checkout_address"));;
		var lane1 = checkoutAddress.billing_addr1;
		var lan2 = checkoutAddress.billing_addr2;
		var city = checkoutAddress.billing_city;
		var postal_code = checkoutAddress.billing_zipcode;
		var billing_phone = checkoutAddress.billing_phone;
		var recipient_name = checkoutAddress.billing_fname + ' ' + checkoutAddress.billing_lname;


		this.payPalConfig = new PayPalConfig(PayPalIntegrationType.ClientSideREST, PayPalEnvironment.Production, {
			commit: true,
			client: {
				//sandbox: 'AWQyGULqIz7CqYioh79Z2DekwFO_sWAfYiyPaMD3UYOZ2nlHKfvDI-mzocXdqDbpBW4XQpe28C2ju3ay',
				production: 'AVYWMUjUgMRsvbZKLG0uTvPBE6cYrouwivD_bpOVTPjgq-UXHwQ-9hLz2c6_3hNRqSTm5BEIBcEQ-0YO' //PayPalEnvironment.Production
			},
			button: {
				label: 'pay', 
				size: 'large',
				color: 'gold',
				shape: 'rect'
			},
			onPaymentComplete: (data, actions) => {
				this.submitOrder(data);
			},
			onCancel: (data, actions) => {
				this._flashMessagesService.show('Error in Payment!, Please try Again.', { cssClass: 'alert-danger', timeout: 5000 });
				this.router.navigate(['/payment']);
			},
			onError: (err) => {
				this._flashMessagesService.show('Error in Payment!, Please try Again.', { cssClass: 'alert-danger', timeout: 5000 });
				this.router.navigate(['/payment']);
			},
			transactions: [{
				amount: {
					currency: 'USD',
					total: this.cart.total
				},
				item_list: {
					"shipping_address": {
						"recipient_name": recipient_name,
						"line1": lane1,
						"line2": lan2,
						"city": city,
						"country_code": countryCode,
						"postal_code": postal_code,
						"phone": billing_phone,
						"state": stateCode
					}
				}
			}]
		});

	}

	submitOrder(paymentData) {

		var data: any = {};
		data.user = this.user;
		data.address = JSON.parse(localStorage.getItem("checkout_address"));
		data.cart = this.cart;
		data.usertime = this.timeZone;
		data.payment = { 'payment_method': 'paypal', 'payment_id': paymentData.paymentID };

		this.orderService.saveOrder(data).subscribe((response: any) => {
			$('span.cartcnt').hide();
			localStorage.removeItem('mycart');
			localStorage.removeItem('checkout_address');
			this._flashMessagesService.show('Thank you for your Order! Your Order Submitted Successfully.', { cssClass: 'alert-success', timeout: 5000 });
			this.router.navigate(['/user/dashboard']);
		});

	}


}
