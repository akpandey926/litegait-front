import { Component, OnInit } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import * as $ from 'jquery';

import { AuthGuard } from '../../services/authguard.service';
import { environment } from '../../../environments/environment';
import { OrderService } from '../../services/order.service';

import * as countries from "./_files/countries.json";
//import * as states  from "./_files/state.json";
import { WebinarService } from '../../services/webinar.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import * as moment from 'moment-timezone';

@Component({
	selector: 'app-checkout',
	templateUrl: './checkout.component.html',
	styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

	cart: any = [];
	env = environment;
	billingForm: FormGroup;
	formError: any = [];
	user;
	couponCode = '';
	couponSuccessMsg = '';
	couponErrorMsg = '';
	userAddress: any;
	show_shipping: boolean = false;
	countryData: any = (countries as any).default;
	timeZone = moment.tz.guess();
	//stateData:  any  = (states  as  any).default;
	states: {}

	constructor(private _flashMessagesService: FlashMessagesService,private authGuard: AuthGuard, private orderService: OrderService, private titleService: Title, private router: Router, private activatedRoute: ActivatedRoute, private webinarService: WebinarService) {
		this.router.routeReuseStrategy.shouldReuseRoute = function () {
			return false;
		};
	}

	ngOnInit() {
		
		this.user = this.authGuard.getUser();
		this.orderService.getUserDefaultAddress(this.user.user_id).subscribe((response: any) => {
			if (response.status == 'success') {
				this.userAddress = response.data;
				if(this.userAddress){
					this.billingForm.controls['billing_fname'].setValue(this.userAddress.billing_fname);
					this.billingForm.controls['billing_lname'].setValue(this.userAddress.billing_lname);
					this.billingForm.controls['billing_phone'].setValue(this.userAddress.billing_phone);
					this.billingForm.controls['billing_email'].setValue(this.userAddress.billing_email);
					this.billingForm.controls['billing_addr1'].setValue(this.userAddress.billing_add1);
					this.billingForm.controls['billing_addr2'].setValue(this.userAddress.billing_add2);
					this.billingForm.controls['billing_country'].setValue(this.userAddress.billing_country);
					this.billingForm.controls['billing_state'].setValue(this.userAddress.billing_state);
					this.billingForm.controls['billing_city'].setValue(this.userAddress.billing_city);
					this.billingForm.controls['billing_zipcode'].setValue(this.userAddress.billing_zipcode);
					// FOr shiping address
					if(this.show_shipping){
						this.billingForm.controls['shipping_fname'].setValue(this.userAddress.shipping_fname);
						this.billingForm.controls['shipping_lname'].setValue(this.userAddress.shipping_lname);
						this.billingForm.controls['shipping_phone'].setValue(this.userAddress.shipping_phone);
						this.billingForm.controls['shipping_email'].setValue(this.userAddress.shipping_email);
						this.billingForm.controls['shipping_addr1'].setValue(this.userAddress.shipping_addr1);
						this.billingForm.controls['shipping_addr2'].setValue(this.userAddress.shipping_addr2);
						this.billingForm.controls['shipping_country'].setValue(this.userAddress.shipping_country);
						this.billingForm.controls['shipping_state'].setValue(this.userAddress.shipping_state);
						this.billingForm.controls['shipping_city'].setValue(this.userAddress.shipping_city);
						this.billingForm.controls['shipping_zipcode'].setValue(this.userAddress.shipping_zipcode);
					}
					
					this.getState(this.userAddress.billing_country);
				}
				
			}
		});


		this.cart = JSON.parse(localStorage.getItem("mycart"));
		this.titleService.setTitle(environment.siteName + ' - Checkout');
		if (this.cart && this.cart.items.length > 0) {
			this.cart.items.forEach((message) => {
				if (message.type != "webinar") {
					this.show_shipping = true;
				}
			});
		}
		this.initBillingForm();
	}

	getState(countryId) {
		if (countryId) {
			localStorage.setItem("countryCode", JSON.stringify(countryId));
			this.orderService.getStates(countryId).subscribe((response: any) => {

				if (response.status == 'success') {
					this.states = response.data;
				} else {
					this.states = null;
				}
			});
		} else {
			this.states = null;
		}
	}
	onChangeCountry(countryId: number) {
		this.getState(countryId);

	}
	onChangeState(stateId: number) {
		localStorage.setItem("stateCode", JSON.stringify(stateId));
	}

	initBillingForm() {
		if (this.show_shipping) {
			this.billingForm = new FormGroup({
				'billing_fname': new FormControl('', [
					Validators.required,
				]),
				'billing_lname': new FormControl(),
				'billing_phone': new FormControl('', [
					Validators.required,
				]),
				'billing_email': new FormControl('', [
					Validators.required,
				]),
				'billing_addr1': new FormControl('', [
					Validators.required,
				]),
				'billing_addr2': new FormControl(),
				'billing_city': new FormControl('', [
					Validators.required,
				]),
				'billing_state': new FormControl('', [
					Validators.required,
				]),
				'billing_country': new FormControl('', [
					Validators.required,
				]),
				'billing_zipcode': new FormControl('', [
					Validators.required,
					Validators.pattern("^[0-9]*$")
				]),

				'shipping_fname': new FormControl('', [
					Validators.required,
				]),
				'shipping_lname': new FormControl(),
				'shipping_phone': new FormControl('', [
					Validators.required,
				]),
				'shipping_email': new FormControl('', [
					Validators.required,
				]),
				'shipping_addr1': new FormControl('', [
					Validators.required,
				]),
				'shipping_addr2': new FormControl(),
				'shipping_city': new FormControl('', [
					Validators.required,
				]),
				'shipping_state': new FormControl('', [
					Validators.required,
				]),
				'shipping_country': new FormControl('', [
					Validators.required,
				]),
				'shipping_zipcode': new FormControl('', [
					Validators.required,
					Validators.pattern("^[0-9]*$")
				]),
			});
		} else {
			this.billingForm = new FormGroup({
				'billing_fname': new FormControl('', [
					Validators.required,
				]),
				'billing_lname': new FormControl(),
				'billing_phone': new FormControl('', [
					Validators.required,
				]),
				'billing_email': new FormControl('', [
					Validators.required,
				]),
				'billing_addr1': new FormControl('', [
					Validators.required,
				]),
				'billing_addr2': new FormControl(),
				'billing_city': new FormControl('', [
					Validators.required,
				]),
				'billing_state': new FormControl('', [
					Validators.required,
				]),
				'billing_country': new FormControl('', [
					Validators.required,
				]),
				'billing_zipcode': new FormControl('', [
					Validators.required,
					Validators.pattern("^[0-9]*$")
				]),
				'shipping_email': new FormControl(),
			});
		}

		var email = localStorage.getItem('email');
		this.billingForm.controls.shipping_email.setValue(email);
		this.billingForm.controls.billing_email.setValue(email);


	}

	sameAsBilling() {

		this.billingForm.controls.shipping_fname.setValue(this.billingForm.controls.billing_fname.value);

		$(document).ready(() => {
			if ($('#same_as_billing').is(":checked")) {
				this.billingForm.controls.shipping_fname.setValue(this.billingForm.controls.billing_fname.value);
				this.billingForm.controls.shipping_lname.setValue(this.billingForm.controls.billing_lname.value);
				this.billingForm.controls.shipping_phone.setValue(this.billingForm.controls.billing_phone.value);
				this.billingForm.controls.shipping_email.setValue(this.billingForm.controls.billing_email.value);
				this.billingForm.controls.shipping_addr1.setValue(this.billingForm.controls.billing_addr1.value);
				this.billingForm.controls.shipping_addr2.setValue(this.billingForm.controls.billing_addr2.value);
				this.billingForm.controls.shipping_city.setValue(this.billingForm.controls.billing_city.value);
				this.billingForm.controls.shipping_state.setValue(this.billingForm.controls.billing_state.value);
				this.billingForm.controls.shipping_state.setValue(this.billingForm.controls.billing_state.value);
				this.billingForm.controls.shipping_country.setValue(this.billingForm.controls.billing_country.value);
				this.billingForm.controls.shipping_zipcode.setValue(this.billingForm.controls.billing_zipcode.value);

				$('.shippingdiv').hide();
			}
			else {
				$('.shippingdiv').show();
			}
		});
	}

	submitAddressForm() {

		if (this.billingForm.valid) {
			localStorage.setItem("checkout_address", JSON.stringify(this.billingForm.value));


			//Check if cart items is zero then it directly order otherwise go to payment page

			if( this.cart.total <= 0){ 
				var data:any={};
				data.user = this.user;
				data.cart = this.cart;
				data.usertime = this.timeZone;
				data.address = JSON.stringify(this.billingForm.value);
				this.webinarService.purchase_webinar_directly(data).subscribe((response:any) => { 
					if (response["status"] == 'success') {
						this._flashMessagesService.show('Item purchased successfully.', { cssClass: 'alert-success', timeout: 5000 });
						$('span.cartcnt').hide();
						localStorage.removeItem('mycart');
						  localStorage.removeItem('checkout_address');
						this.router.navigate(['/user/webinar']);
					} else {
					   this._flashMessagesService.show(response["msg"], { cssClass: 'alert-danger', timeout: 5000 });
					}            
				});
			}else{
				this.router.navigate(['/payment']);
			}
			
		} else {
			this.validateAllFormFields(this.billingForm);
		}
	}

	validateAllFormFields(formGroup: FormGroup) {
		Object.keys(formGroup.controls).forEach(field => {
			const control = formGroup.get(field);
			if (control instanceof FormControl) {
				control.markAsTouched({ onlySelf: true });
			} else if (control instanceof FormGroup) {
				this.validateAllFormFields(control);
			}
		});
	}

	applyCoupon() {

		if (this.couponCode == "") {
			this.couponErrorMsg = "Please Enter Coupon Code";
			return true;
		}

		this.orderService.checkCouponCode(this.couponCode).subscribe((response: any) => {

			if (response.status == 'success') {
				this.cart.discount = response.discount;
				this.cart.discountcode = response.discountcode;
				
				this.cart.total = (this.cart.subtotal - response.discount).toFixed(2);
				if(this.cart.total < 0){
					this.cart.total = 0;
				}else{
					this.cart.total = (this.cart.subtotal - response.discount).toFixed(2);
				}
				//console.log(this.cart.total);return false;
				this.couponSuccessMsg = response.msg;
				this.couponErrorMsg = '';
				localStorage.setItem("mycart", JSON.stringify(this.cart));
				return true;
			} else {
				this.cart.discount = 0;
				this.cart.discountcode = '';
				this.cart.total = this.cart.subtotal;
				this.couponErrorMsg = response.msg;
				this.couponSuccessMsg = '';

				localStorage.setItem("mycart", JSON.stringify(this.cart));
				return true;
			}

		});

	}
}
