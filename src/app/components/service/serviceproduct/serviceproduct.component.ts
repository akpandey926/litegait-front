import { Component, OnInit, ViewChild, ElementRef, TemplateRef} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

import { environment } from '../../../../environments/environment';
import { ProductService } from '../../../services/product.service';
import { ServicesService } from '../../../services/services.service';

@Component({
  selector: 'app-serviceproduct',
  templateUrl: './serviceproduct.component.html',
  styleUrls: ['./serviceproduct.component.css']
})
export class ServiceproductComponent implements OnInit {

	env = environment;
    products: any = []; 
	documents:any = [];
	 slug = null;   

    constructor(private servicesService: ServicesService,private productService: ProductService, private router: Router, private activatedRoute: ActivatedRoute, private titleService: Title) {        
    }

    ngOnInit() {
        this.slug = this.activatedRoute.snapshot.paramMap.get('slug');
        /* this.productService.getProduct(slug).subscribe((response: any) => {
            this.product = response.data;                                  
            this.titleService.setTitle(environment.siteName + ' - Service -' + response.data.meta_title);    
        }); */

        this.servicesService.getDocumentList().subscribe((response:any) => {   			 			
  			this.products = response.data;  			
  			this.documents = response.documents;  			  			
        });  
    }

    getKeys(documents){

    	var arr= Object.keys(documents);
    	var index = arr.indexOf("_id");
	    if (index !== -1) {
	        arr.splice(index, 1);
	    }  
    	var index = arr.indexOf("product_id");
	    if (index !== -1) {
	        arr.splice(index, 1);
	    } 
	    var index = arr.indexOf("created_at");
	    if (index !== -1) {
	        arr.splice(index, 1);
	    } 
	    var index = arr.indexOf("updated_at");
	    if (index !== -1) {
	        arr.splice(index, 1);
	    } 
    	return arr;
    }

    showKeys(key){
    	var res = key.replace("_", " ");
    	return res;
    }

}
