import { Component, OnInit,ChangeDetectorRef } from '@angular/core';
import { Title,Meta }  from '@angular/platform-browser';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { HttpClient} from '@angular/common/http';

import { environment } from '../../../../environments/environment';
import { ServicesService } from '../../../services/services.service';


@Component({
  selector: 'app-servicedocuments',
  templateUrl: './servicedocuments.component.html',
  styleUrls: ['./servicedocuments.component.css']
})
export class ServicedocumentsComponent implements OnInit {

	env = environment;
	products:any = [];
	documents:any = [];    
	categories:any = [];    
    keyword: any = [];
    p = 1;
    itemsPerPage = 10;
    filterMetadata = { count: 0 }; 
    catHasProduct :any = [];
    slug = null;
    title ='Product Information';
    serialno='';
	error='';
	res:any;

    constructor(private servicesService: ServicesService, private titleService: Title,private router: Router, private activatedRoute: ActivatedRoute,private http:HttpClient,private changeDetectorRef: ChangeDetectorRef) { 
  	}

    ngOnInit() {
    	this.slug = this.activatedRoute.snapshot.paramMap.get('category');    	
    	this.titleService.setTitle(environment.siteName + ' - Product Information');
    	this.servicesService.getDocumentList().subscribe((response:any) => { 
  			this.products = response.data;  			
			this.documents = response.documents;  		
  			this.categories = response.categories;
			this.changeDetectorRef.detectChanges();
  			if(this.slug != null) 			
  			{	  				
  				var slugcategory: any = [];;
  				for(var key in this.categories)
  				{
  					if(this.categories[key]['slug'] == this.slug)
  					{
  						slugcategory[0] = this.categories[key];
  					}
  				}
  				this.categories = slugcategory;
  				var prslug ='';
  				var prcnt = 0;
  				for(var key in this.products)
  				{
  					if(this.products[key]['category'] == slugcategory[0]['_id'])
  					{
  						prcnt++;
  						prslug = this.products[key]['slug'];
  					}
  				}
  				if(prcnt == 1)
  				{
  					this.router.navigate(['/service/product/'+prslug]);
				  }
				  this.changeDetectorRef.detectChanges();
  			}
  			
        });  
    }

    getKeys(documents){

    	var arr= Object.keys(documents);
    	var index = arr.indexOf("_id");
	    if (index !== -1) {
	        arr.splice(index, 1);
	    }  
    	var index = arr.indexOf("product_id");
	    if (index !== -1) {
	        arr.splice(index, 1);
	    } 
	    var index = arr.indexOf("created_at");
	    if (index !== -1) {
	        arr.splice(index, 1);
	    } 
	    var index = arr.indexOf("updated_at");
	    if (index !== -1) {
	        arr.splice(index, 1);
	    } 
    	return arr;
    }

    showKeys(key){
		var res = key.replace("_", " ");
    	return res;
    }

    updatecatHasProduct(catid,val)
    {
    	if(this.catHasProduct[catid] == undefined)
    	{
    		this.catHasProduct[catid] = val; 
    	}	
    	return true;
    }
    checkcatHasProduct(catid)
    {    	
    	if(this.catHasProduct[catid] == true)    	
    		return true;
    	else
    		return false;
    }

    searchBySerialNo(){    	

    	this.http.post(environment.apiUrl+"/product/searchbyserialno",{'serialno':this.serialno}).subscribe((response) => {      			
			this.res = response;			
			if(this.res.status == 'success') 			
			{
				this.router.navigate(['/service/product/'+this.res.data.slug]);
			}
			else if(this.res.serial != undefined && this.res.serial.name !='') 			
			{
				this.router.navigate(['/page/service-contact-us']);
			}

			this.error = this.res.msg;
        });
    }


}
